from flask import render_template
import cx_Oracle
import os
import utils
from passlib.hash import sha256_crypt
from datetime import datetime, timedelta
import functools
import subprocess
import requests
import os.path
from os import path

## @param b: Boolean: True if you want the entire connection (for delete / updating / insert queries), False if you just want the cursor (select queries) 
def newDatabaseConnection(b=False):
    dsn_tns = cx_Oracle.makedsn(os.getenv('DB_HOST_IP'),os.getenv('DB_PORT'),service_name=os.getenv('DB_SERVICE_NAME'))
    connection = cx_Oracle.connect(os.getenv('DB_USER'),os.getenv('DB_PASSWD'),dsn_tns)
    return connection if b else connection.cursor()

class SIGNUP():
    def __init__(self):
        pass

    def checkUsernameExists(self, username):
        c = newDatabaseConnection(False)
        query_string = "select * from customers c where c.username = :username"
        c.execute(query_string, [username])
        user = c.fetchone()
        if user is None:
            c.close()
            return False
        c.close()
        return True

    def verifyPassword(self, password):
        return True

    def hashPassword(self, password):
        return sha256_crypt.encrypt(password)

    def createUser(self, user):
        success, error = self.checkUser(user)
        if success:
            con = newDatabaseConnection(True)
            add_user = "INSERT into CUSTOMERS (first_name, last_name, username, hashed_password, profile_picture) VALUES (:fn, :ln, :un, :pw, :pp)"
            hashed_password = self.hashPassword(user['password'])
            profile_picture = 0
            cur = con.cursor()
            print(user['firstname'], user['lastname'], user['username'], hashed_password)
            # create row in customers table
            cur.execute(add_user, [user['firstname'], user['lastname'], user['username'], hashed_password, profile_picture])
            con.commit()
            query_string = "SELECT c.cust_id from CUSTOMERS c where c.username = :un"
            cur.execute(query_string, [user['username']])
            cust_id = cur.fetchone()[0]
            cur.close()
            # generate qr code
            self.generateQR(str(cust_id))
            return True, {}
        else:
            return False, error

    def generateQR(self, cid):
        print('cust_id', cid)
        API_URL = "https://api.qrserver.com/v1/create-qr-code/?size=150x150&data="
        SITE_URL = "http://54.145.160.33:8503/"
        URL = API_URL + SITE_URL + 'qr_validate?id=' + cid
        r = requests.get(url = URL) 
        with open('./static/data/qr_codes/' + cid + '.png','wb') as f:
            f.write(r.content)

    def checkUser(self, user):
        error = {}
        if self.checkUsernameExists(user['username']):
            error['code'] = 1
            error['message'] = "Username already exists"
            return False, error
        if not self.verifyPassword(user['password']):
            error['code'] = 2
            error['message'] = "Password is too short"
            return False, error
        return True, {}

class LOGIN():
    def __init__(self):
        pass
 
    def confirmQR(self, cid):
        QR_PATH = "./static/data/qr_codes/"
        if not path.exists(QR_PATH + str(cid)):
            self.regenerateQR(str(cid))

    def regenerateQR(self, cid):
        print('cust_id', cid)
        API_URL = "https://api.qrserver.com/v1/create-qr-code/?size=150x150&data="
        SITE_URL = "http://54.145.160.33:8503/"
        URL = API_URL + SITE_URL + 'qr_validate/' + cid
        r = requests.get(url = URL) 
        with open('./static/data/qr_codes/' + cid + '.png','wb') as f:
            f.write(r.content)

    def verifyUser(self, username, password):
        c = utils.newDatabaseConnection() 
        customer = utils.customerOrBarExists(c, "customers", username)
        if customer is None:
            bar = utils.customerOrBarExists(c, "bars", username)
            if bar is None:
                return {'success': False, 'error': 'username not found'} 
            else: 
                if utils.validateCustomerOrBar(bar, password):
                    return {'success': True, 'type': 'bar', 'info': bar } 
                else:
                    return {'success': False, 'error': 'incorrect password'}     
        else: 
            if utils.validateCustomerOrBar(customer, password):
                return { 'success': True, 'type': 'customer', 'info': customer } 
            else:
                return {'success': False, 'error': 'incorrect password'}


