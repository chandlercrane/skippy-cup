# Skippy Cup

This project is our final project for our advanced databases course.

Course Website: [Advanced Database Projects](https://www3.nd.edu/~rbualuan/courses/advdb20/)

In order to get the project's complete functionality, complete following items:

*The env/ folder should contain all the requirements for the project, since it's a virtual environment. In order to be able to run the app, you have to be inside the virtual environment: use source env/bin/activate.

Add the following lines to your ~/.bash_profile for necessity and security:
```
export DB_HOST_IP=<YOUR-DB-IP-ADDRESS>
export DB_PORT='1521'
export DB_USER=guest
export DB_PASSWD=guest
export DB_SERVICE_NAME=XE
export ORACLE_LIB=$ORACLE_HOME/lib
export LD_LIBRARY_PATH=$PATH:$ORACLE_LIB
```

Activate the Virtual Environment (env):
```
. env/bin/activate
```

Ensure required libraries are installed:
```
sudo python3 -m pip install -r requirements.txt
```

Run the app:
```
python3 app.py
```


**What's included so far:**

1. [app.py]: A simple route to the root of the app (/), running on your Amazon instance IP at port 8100. This handler fcn (def index():) is executed when &lt;IP-ADDRESS&gt;:8100 is accessed from a browser.
2. [app.py]: A sample database connection. Uses environmental variables above to make a connection to the OracleDB.
3. [app.py] From here, it is extremely easy to work with the result of a query. Strangely you can't use the connection (c) as a whole, but iterate through each row in it. So I did a list comprehension. 
4. [app.py] return render_template('index.html', results=results). Aka, send the browser the HTML located at templates/index.html, with the data stored in the variable results.
5. [templates/base.html]: this shows how we don't have to reuse code. I'm using this as the base page, containing code that all the real pages we'll access use. Anytime there is information relative to a specific page, a {% block ____ %}{% endblock %} tag is inserted.
6. [templates/index.html]: this file, shows how to of fill in the template gaps. Notice everything, but especially at the bottom where there is a for loop accessing the variable **results**, which was passed from app.py. The first line at the top of this file ({% extends 'base.html' %}) shows where the base code comes from.
7. [static/*]: This folder contains our css and js, the locations of which are referenced in templates/index.html. If either don't seem to be working, try an Empty Cache and Hard Reload on your browser.