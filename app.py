from flask import Flask, render_template, url_for, request, session, redirect, jsonify, make_response
from user import *
import utils, os, functools, json
from datetime import datetime
from apscheduler.scheduler import Scheduler 
from passlib.hash import sha256_crypt

app = Flask(__name__)
sched = Scheduler()
sched.start()
sched.add_cron_job(func=utils.delete_previous_day_purchases, hour=16, minute=0, second=0)
# ^^ At noon every day, all purchases that were started but never finished are deleted from the db.
# See utils.delete_previous_day_purchases for details. 
bar_pictures = os.path.join(os.getcwd(), 'static/data/bars')


@app.route('/')
def index():
    c = utils.newDatabaseConnection(False) 
    return render_template('index.html')


app.config["SECRET_KEY"] = "ilxg8kSoztXX3lQmAJzwqQ"

@app.route('/signup', methods=["GET", "POST"])
def signup():
    if request.method == "POST": 
        user = {}
        user['username'] = request.json["username"].strip()
        user['password'] = request.json["password"].strip()
        user['email'] = request.json["email"].strip()
        user['firstname'] = request.json["first_name"].strip()
        user['lastname'] = request.json["last_name"].strip()
        user_keys = list(user.keys())
        print(user_keys) 
        filled_out = functools.reduce(lambda x,y: x and len(user[y]) > 0, user_keys, len(user[user_keys[0]]) > 0)
        if not filled_out:
            return jsonify({"success": "no", "error": "One or more fields were left blank."})
        elif len(user['username']) < 6:
            return jsonify({"success": "no", "error": "Username must be at least 6 characters ."}) 
        elif len(user['password']) < 8 or not utils.someLettersAndNumbers(user['password']):
            return jsonify({"success" : "no", "error": "Password must be at least 8 characters long and contain letters and numbers."})

        signup = SIGNUP()
        success, error = signup.createUser(user)
        print(success, error)
        if success:
            session['USERNAME'] = user['username']
            session['TYPE'] = "customer"
            session['NAME'] = user['firstname'] + ' ' + user['lastname']
            session['HAS_PROFILE_PICTURE'] = False
            return jsonify({"success": "yes"})
        else:
            return jsonify({"success": "no", "error": "Username already exists."}) 
    return render_template('signup.html')

@app.route('/create-checkin', methods=["POST"])
def createCheckIn():
    username = session.get("USERNAME", None) 
    user_type = session.get("TYPE", None) 
    if username is None:
        return redirect(url_for('index')) 
    if user_type != 'customer':
        return redirect(url_for('bartender'))
    if request.method == "POST":
        req = request.form
        checkin = {}
        bar = json.loads(req.get("bar"))
        plan = json.loads(req.get("plan"))
        print(bar)
        print(plan)
        start = datetime.now()
        checkin['bar_id'] = bar['bar_id']
        checkin['start_date'] = utils.formatDate(start)
        checkin['end_date'] = utils.formatDate(start + timedelta(days=plan['delta']))
        checkin['price'] = plan['price']
        checkin['allbars'] = plan['allbars']
        checkin['duration'] = plan['duration']
        checkin['duration_unit'] = plan['duration_unit']
        print(checkin)
        success = utils.createCheckin(checkin, session)
        if success:
            session['CHECKED_IN'] = True
            return redirect(url_for("customer"))
        else:
            return redirect(url_for('checkin'))
    return redirect(url_for('checkin'))

@app.route('/login', methods=["GET", "POST"])
def login():
    if request.method == "POST":
        username = request.json["username"]
        password = request.json["password"]
        login = LOGIN()
        result = login.verifyUser(username, password)
        print(result)
        if result['success']:          
            if result['type'] == 'customer':
                login.confirmQR(result['info'][0])
            sessionDict = utils.buildSessionDict(result)
            for key, value in sessionDict.items():
                session[key] = value
        if not result['success']:
            return {'success': 'no'} 
        elif result['type'] == 'customer':
            t = utils.redirectLogin(session)
            session["CHECKED_IN"] = t == "customer"
            return {'success': 'yes', 'type': t}
        else:
            return {'success': 'yes', 'type': 'bar'} 
    return render_template('login.html')

@app.route('/logout')
def logout():
    session.clear() # destroys all session variables. 
    return redirect(url_for('index'))

@app.route('/confirm-purchase', methods=["POST"])
def confirmCheckout():
    username = session.get("USERNAME", None) 
    user_type = session.get("TYPE", None) 
    if username is None:
        return redirect(url_for('index')) 
    if user_type != 'customer':
        return redirect(url_for('bartender'))
    if request.method == "POST":
        req = request.form
        print(req)
        cart_id = req.get("cart_id")
        tip = req.get("tip")
        utils.cartToPurchase(cart_id, tip)
    return redirect(url_for('orders')) 

@app.route('/update-drink', methods=["POST"])
def updateDrink():
    username = session.get("USERNAME", None) 
    user_type = session.get("TYPE", None) 
    if username is None:
        return redirect(url_for('index')) 
    if user_type != 'customer':
        return redirect(url_for('bartender'))
    if request.method == "POST":
        req = request.form
        print(req)
        update = {}
        update["quantity"] = req.get("quantity")
        update["drink_id"] = req.get("drink_id")
        update["cart_id"] = req.get("cart_id")
        updateType = req.get("button")
        if updateType == '0':
            utils.deleteDrink(update)
        if updateType == '1':
            utils.updateDrink(update) 
    return redirect(url_for('checkout'))

@app.route('/add-to-cart', methods=["POST"])
def addToCart():
    username = session.get("USERNAME", None) 
    user_type = session.get("TYPE", None) 
    if username is None:
        return redirect(url_for('index')) 
    if user_type != 'customer':
        return redirect(url_for('bartender'))
    if request.method == "POST":
        req = request.form
        print(req)
        order = {}
        order['drink'] = json.loads(req.get("drink"))
        order['bar'] = json.loads(req.get("bar"))
        order['note'] = req.get("note")
        order['quantity'] = req.get("quantity")
        success = utils.addToCart(session, order)
        if success:
            return redirect(url_for("customer"))
        else:
            return redirect(url_for('customer'))
    return redirect(url_for('customer')) 

@app.route('/orders', methods=["GET"])
def orders():
    username = session.get("USERNAME", None) 
    user_type = session.get("TYPE", None) 
    if username is None:
        return redirect(url_for('index')) 
    if user_type != 'customer':
        return redirect(url_for('bartender'))
    return utils.ordersData(session)

@app.route('/checkout', methods=["GET"])
def checkout():
    username = session.get("USERNAME", None) 
    user_type = session.get("TYPE", None) 
    if username is None:
        return redirect(url_for('index')) 
    if user_type != 'customer':
        return redirect(url_for('bartender'))
    return utils.checkoutData(session)

@app.route('/checkin', methods=["GET"])
def checkin():
    username = session.get("USERNAME", None) 
    user_type = session.get("TYPE", None) 
    if username is None:
        return redirect(url_for('index')) 
    if user_type != 'customer':
        return redirect(url_for('bar_dashboard'))
    return utils.checkinData(session)

@app.route('/customer', methods=["GET"])
def customer():
    username = session.get("USERNAME", None) 
    user_type = session.get("TYPE", None) 
    if username is None:
        return redirect(url_for('index')) 
    if user_type != 'customer':
        return redirect(url_for('bar_dashboard'))
    return utils.custDashboardData(session)

@app.route('/bartender', methods=["GET"])
def bartender():
    username = session.get("USERNAME", None) 
    user_type = session.get("TYPE", None) 
    if username is None:
        return redirect(url_for('index')) 
    if user_type != 'bar':
        return redirect(url_for('customer'))
    earliest_drinks_date = utils.getEarliestDrinksDate() 

    return utils.barDashboardData('bartender.html', session)

@app.route('/update_bartender', methods=["GET"])
def update_bartender():
    username = session.get("USERNAME", None) 
    user_type = session.get("TYPE", None) 
    if username is None:
        return redirect(url_for('index')) 
    if user_type != 'bar':
        return redirect(url_for('customer'))
       
    return utils.barDashboardData('bartending_data.html', session) 


@app.route('/confirm_dropped_order/<pid>', methods=["DELETE"])
def process_dropped_order(pid):
    user_type = session.get("TYPE", None) 
    if user_type != 'bar':
        return redirect(url_for('customer'))
    conn = utils.newDatabaseConnection(True)
    c = conn.cursor()
    c.execute('delete from purchases where purchase_id = {}'.format(pid))
    c.execute('delete from purchase_drinks where purchase_id = {}'.format(pid))
    conn.commit() # necessary
    return jsonify({'success': 'ok'})

@app.route('/prepare_order/<pid>', methods=["POST"])
def prepare_order(pid):
    user_type = session.get("TYPE", None) 
    if user_type != 'bar':
        return redirect(url_for('customer'))
    conn = utils.newDatabaseConnection(True)
    c = conn.cursor()
    c.execute('update purchases set drink_prepared = 1 where purchase_id = {}'.format(pid))
    conn.commit()
    return jsonify({'success': 'yes'})

@app.route('/serve_order/<pid>', methods=["POST"])
def serve_order(pid):
    user_type = session.get("TYPE", None) 
    if user_type != 'bar':
        return redirect(url_for('customer'))
    conn = utils.newDatabaseConnection(True)
    c = conn.cursor()
    c.execute('update purchases set drink_picked_up = 1 where purchase_id = {}'.format(pid))
    conn.commit()
    return jsonify({'success': 'yes'})
    
@app.route('/bar_dashboard', methods=["GET"])
def bar_dashboard():
    username = session.get("USERNAME", None) 
    user_type = session.get("TYPE", None) 
    if username is None:
        return redirect(url_for('index'))
    if user_type != 'bar':
        return redirect(url_for('customer'))
    bar = utils.buildBarVariable(session)
    print("bar={}".format(bar))
    return render_template('bar_dashboard.html', bar=bar)

@app.route('/cust_settings', methods=["GET"])
def cust_settings():
    username = session.get("USERNAME", None)
    user_type = session.get("TYPE", None) 
    if username is None:
        return redirect(url_for('index'))
    if user_type != 'customer':
        return redirect(url_for('bartender'))
    return "customer settings"

@app.route('/update_menu', methods=["GET"])
def update_menu():
    username = session.get("USERNAME", None) 
    user_type = session.get("TYPE", None) 
    if username is None:
        return redirect(url_for('index'))
    if user_type != 'bar':
        return redirect(url_for('customer'))
    ## may have to do the cache thing here
    return utils.updateMenuData("update_menu.html", "name ASC", session) 

@app.route('/update_menu_sequence', methods=["POST"])
def update_menu_sequence():
    username = session.get("USERNAME", None) 
    user_type = session.get("TYPE", None) 
    if username is None:
        return redirect(url_for('index'))
    if user_type != 'bar':
        return redirect(url_for('customer')) 

    sql_string = utils.findMenuOrder(request.json["sorting_method"])
    return utils.updateMenuData("update_menu_data.html",sql_string, session)

@app.route('/earnings', methods=["GET"])
def earnings():
    username = session.get("USERNAME", None) 
    user_type = session.get("TYPE", None) 
    if username is None:
        return redirect(url_for('index'))
    if user_type != 'bar':
        return redirect(url_for('customer'))
    return render_template("earnings.html")

@app.route('/settings', methods=["GET"])
def settings():
    username = session.get("USERNAME", None) 
    user_type = session.get("TYPE", None) 
    if username is None:
        return redirect(url_for('index'))
    c = utils.newDatabaseConnection(False)
    c.execute("select * from bars where bar_id = :bid", [session['ID']])
    c.rowfactory = utils.makeDictFactory(c) 
    bar_draft = c.fetchone()
    print(bar_draft) 
    bar = { k: v if v != None else "" for (k, v) in bar_draft.items()}
    response = make_response(render_template('settings.html', bar=bar))
    response.cache_control.max_age = 0 
    response.cache_control.must_revalidate = True
    return response;

@app.route('/bar_username_update_attempt/<bid>', methods=['PUT'])
def bar_username_update_attempt(bid): 
    username = session.get("USERNAME", None) 
    user_type = session.get("TYPE", None) 
    if username is None:
        return redirect(url_for('index'))
    if user_type != 'bar':
        return redirect(url_for('customer'))
    conn = utils.newDatabaseConnection(True)
    c = conn.cursor()
    new_username = request.json["new_username"]
    c.execute('select bar_id from bars where username = :uname and bar_id != :bid', [new_username, bid])
    results = [row for row in c]
    if len(results) > 0:
        return jsonify({'success': 'no', 'error': 'username already exists'}) 
    else:
        c.execute('update bars set username = :uname where bar_id = :bid', [new_username, bid]) 
        conn.commit()
        return jsonify({'success': 'yes'})


@app.route('/settings_update/<bid>', methods=['PUT'])
def settings_update(bid):
    username = session.get("USERNAME", None)
    if username is None:
        return redirect(url_for('index'))
    conn = utils.newDatabaseConnection(True)
    c = conn.cursor()
    col = request.json["column"]
    val = request.json["value"]
    print(col, val)
    c.execute('update bars set {} = :value where bar_id = :bid'.format(col), [val, bid])
    conn.commit()
    return jsonify({'success': 'yes'})

@app.route('/password_update/<bid>', methods=["PUT"])
def password_update(bid):
    username = session.get("USERNAME", None)
    if username is None:
        return redirect(url_for('index'))
    conn = utils.newDatabaseConnection(True)
    c = conn.cursor()
    old_password = request.json["old_password"]
    new_password = request.json["new_password"]
    c.execute('select hashed_password from bars where bar_id = :bid', [bid])
    old_hashed_password = [row for row in c][0][0]
    if sha256_crypt.verify(old_password, old_hashed_password):
        new_encrypted_password = sha256_crypt.encrypt(new_password)
        c.execute('update bars set hashed_password = :npw where bar_id = :bid', [new_encrypted_password, bid])
        conn.commit()
        return jsonify({'success': 'yes'})
    return jsonify({'success': 'no'}) 

@app.route('/bar_profile_picture_update/<bid>', methods=["POST"])
def bar_profile_picture_update(bid):
    conn = utils.newDatabaseConnection(True)
    c = conn.cursor()
    c.execute('update bars set profile_picture = 1 where bar_id = :bid', [bid]);
    conn.commit()
    img_file = request.files["profile"]
    img_file.save(os.path.join(bar_pictures, str(bid) + '.jpg')) 
    utils.validateImageFile(bar_pictures, bid)
    return jsonify({"success": "ok"})

@app.route('/get_existing_item_data/<drink_id>', methods=["GET"])
def get_existing_item_data(drink_id):
    c = newDatabaseConnection(False)
    c.execute("select i.item_id, i.name, i.popular, i.profile_picture, i.brand, i.drink_type, LISTAGG(NVL(TO_CHAR(ing.ingredient_id) || '/' || ing.name, '#'), ',') WITHIN GROUP (ORDER BY ing.name) ingredients_list FROM items i INNER JOIN item_ingredients ii ON i.item_id = ii.item_id INNER JOIN ingredients ing ON ii.ingredient_id = ing.ingredient_id WHERE i.item_id = (select item_id from drinks where drink_id = :did1) GROUP BY i.item_id, i.name, i.popular, i.profile_picture, i.brand, i.drink_type UNION select i.item_id, i.name, i.popular, i.profile_picture, i.brand, i.drink_type, '#' from items i where i.item_id not in (select item_id from item_ingredients ii) and i.item_id = (select item_id from drinks where drink_id =:did2)", [drink_id, drink_id])
    c.rowfactory = utils.makeDictFactory(c)
    item_data = c.fetchone()
    c.execute("select drink_id, price, size_ounces, container, note FROM drinks where drink_id = :did", [drink_id])
    c.rowfactory = utils.makeDictFactory(c)
    drink_data = c.fetchone()
    drink_data['PRICE'] = utils.dollarStringFromFloat(drink_data['PRICE'])
    c.execute("select special_price, special_note, day, start_date, end_date, start_time, end_time from specials where drink_id = :did", [drink_id])
    special_data = list(map(lambda x: {'SPECIAL_PRICE': utils.dollarStringFromFloat(x[0]), 'SPECIAL_NOTE': x[1], 'DAY': x[2], 'START_DATE': utils.datetimeToDatetimeString(x[3]), 'END_DATE': utils.datetimeToDatetimeString(x[4]), 'START_TIME': utils.datetimeToTimeString(x[5]), 'END_TIME': utils.datetimeToTimeString(x[6])}, [row for row in c]))
    print("returning: {}, {}, {}".format(item_data, drink_data, special_data))
    return jsonify({'item_exists': True, 'drink_exists': True, 'item_data': item_data, 'drink_data': drink_data, 'special_data': special_data})

@app.route('/get_all_items', methods=["GET"])
def get_all_items():
    c = newDatabaseConnection(False)
    c.execute("select item_id, name, profile_picture FROM items where popular = 1")
    c.rowfactory = utils.makeDictFactory(c)
    item_data = c.fetchall() 
    return jsonify({'success': 'ok', 'item_data': item_data })

@app.route('/update_menu_item_information', methods=["PUT"])
def update_menu_item_information():
    conn = newDatabaseConnection(True) 
    c = conn.cursor()
    item_data = request.json["item_data"]
    drink_data = request.json["drink_data"]
    special_data = request.json["special_data"]
    
    print("***item_data: {}\n\ndrink_data: {}\n\nspecial_data:{}\n\n***".format(item_data, drink_data, special_data))

    if request.json["item_exists"] and item_data['POPULAR']:
        pass
        ## update items;
        
    elif not request.json["item_exists"]:
        pass
        ## insert into items;

    if request.json["drink_exists"]:
        c.execute("update drinks set PRICE = :p, size_ounces = :o, container = :c, note = :n where drink_id = :did", [drink_data["PRICE"], drink_data["SIZE_OUNCES"], drink_data["CONTAINER"], drink_data["NOTE"], drink_data["DRINK_ID"]])
        c.execute('delete from specials where drink_id = :did', [drink_data['DRINK_ID']])
    else:
        c.execute("insert into drinks (BAR_ID, ITEM_ID, PRICE,SIZE_OUNCES,CONTAINER,NOTE)  SELECT :bid, :iid, :p, :so, :c, :n FROM dual", [session['ID'], item_data['ITEM_ID'], drink_data["PRICE"], drink_data["SIZE_OUNCES"], drink_data["CONTAINER"], drink_data["NOTE"]])
        conn.commit()
        c.execute("select MAX(drink_id) FROM drinks")
        drink_data["DRINK_ID"] = c.fetchone()[0]
        print("new drink id is: {}".format(drink_data["DRINK_ID"]))
         
    if len(special_data) > 0:
        query_string = "insert all "
        query_array = []
        for s in range(len(special_data)):
            temporary = "DAY" not in special_data[s] 
            columns = "DRINK_ID, SPECIAL_PRICE, SPECIAL_NOTE, START_DATE, END_DATE" if temporary  else "DRINK_ID, SPECIAL_PRICE, SPECIAL_NOTE, DAY, START_TIME, END_TIME"
            n_values = 5 if temporary else 6
            values = ", ".join(":d" + str(6*s + i) for i in range(n_values))  
            query_string += "into specials (" + columns + ") VALUES (" + values + ") "  
            query_array.extend([drink_data["DRINK_ID"], special_data[s]["SPECIAL_PRICE"], special_data[s]["SPECIAL_NOTE"]])
            if not temporary:
                start = utils.timeStringToDatetime(special_data[s]['START_TIME']) if special_data[s]['START_TIME'] else ""
                end = utils.timeStringToDatetime(special_data[s]['END_TIME']) if special_data[s]['END_TIME'] else ""
                query_array.extend([special_data[s]['DAY'], start, end])
            else:
                query_array.extend([utils.datetimeStringToDatetime(special_data[s]['START_DATE']), utils.datetimeStringToDatetime(special_data[s]['END_DATE'])])
        query_string += ' select * from dual'
        print(query_string, query_array) 
        c.execute(query_string, query_array)
         
    conn.commit()
    return jsonify({"success": "yes"})


@app.route('/qr_validate/<cid>')
def qr_validate(cid):
    username = session.get("USERNAME", None)
    user_type = session.get("TYPE", None)
    if username is None:
        return redirect(url_for('index'))
    if user_type != 'bar':
        return redirect(url_for('customer'))
    c = newDatabaseConnection(False)
    d = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    print(d)
    c.execute("SELECT cu.cust_id, cu.username, cu.first_name, cu.last_name, cu.profile_picture, NVL(tmp.ct, 0) ct FROM customers cu LEFT OUTER JOIN (select DISTINCT ch.cust_id, count(*) ct from checkins ch where TO_TIMESTAMP(:d1,'YYYY-MM-DD HH24:MI:SS') > ch.start_date and TO_TIMESTAMP(:d2,'YYYY-MM-DD HH24:MI:SS') < ch.end_date and ch.bar_id = :bid GROUP BY ch.cust_id) tmp on cu.cust_id = tmp.cust_id WHERE cu.cust_id = :cid", [d,d,session["ID"], cid])
    c.rowfactory = utils.makeDictFactory(c)
    customer=c.fetchone()
    print(customer)

    return render_template("qr_validator.html", customer=customer)


@app.route('/get_earnings', methods=["PUT"])
def get_earnings():
    print(request.json["chart_range"])
    c = newDatabaseConnection(False)
    middle = utils.getChartRange(request.json["chart_range"])

    c.execute("select count(*) total_purchases, sum(final_cost) total_purchase_revenue, avg(final_cost) avg_purchase_revenue, sum(final_tip) total_tip_revenue, avg(final_tip) avg_tip_revenue, sum(final_cost + final_tip) total_revenue, avg(final_cost + final_tip) avg_revenue from purchases where " + middle + " bar_id = :bid", [session["ID"]])
    c.rowfactory = utils.makeDictFactory(c)
    tabular_statistics = c.fetchone()
    for k in tabular_statistics.keys():
        if k != "TOTAL_PURCHASES":
            tabular_statistics[k] = utils.dollarStringFromFloat(tabular_statistics[k])

    c.execute("select p.purchase_method, ROUND(100*count(*) / (select count(*) from purchases where " + middle + " bar_id = :bid),1) FREQUENCY from  purchases p LEFT JOIN (select distinct purchase_method from purchases) tmp on tmp.purchase_method = p.purchase_method WHERE " + middle + " bar_id = :bid group by p.purchase_method order by p.purchase_method", [session["ID"], session["ID"]])
    c.rowfactory = utils.makeDictFactory(c)
    frequencies = c.fetchall()
    print(frequencies)
    tabular_statistics["CARD_FREQUENCY"] = frequencies[0]["FREQUENCY"]
    tabular_statistics["PAYPAL_FREQUENCY"] = frequencies[1]["FREQUENCY"]
    tabular_statistics["VENMO_FREQUENCY"] = frequencies[2]["FREQUENCY"]
 
    c.execute("select CASE WHEN extract(hour from order_time) >= 12 THEN TO_CHAR(order_time, 'MM/DD') ELSE TO_CHAR(order_time - 1, 'MM/DD') END night_date, COUNT(*) as n_purchases FROM purchases WHERE " + middle + " bar_id = :bid GROUP BY CASE WHEN extract(hour from order_time) >= 12 THEN TO_CHAR(order_time, 'MM/DD') ELSE TO_CHAR(order_time - 1, 'MM/DD') END order by NIGHT_DATE ASC", [session["ID"]])
    c.rowfactory = utils.makeDictFactory(c)
    graphical_statistics = c.fetchall() 
    return jsonify({"TABLE": tabular_statistics, "GRAPH": graphical_statistics})

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=8503)
