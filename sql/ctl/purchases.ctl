load data 
insert into table purchases 
fields terminated by "," 
trailing nullcols
(purchase_id, cust_id, bar_id, order_time TIMESTAMP 'YYYY-MM-DD HH24:MI:SS', final_cost, final_tip, purchase_method, customer_ordered, drink_prepared, drink_picked_up)
