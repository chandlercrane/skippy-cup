load data 
insert into table specials 
fields terminated by "," 
trailing nullcols
(special_id, drink_id, special_price, special_note, start_date TIMESTAMP 'YYYY-MM-DD HH24:MI:SS', end_date TIMESTAMP 'YYYY-MM-DD HH24:MI:SS', day, start_time TIMESTAMP 'HH24:MI:SS', end_time TIMESTAMP 'HH24:MI:SS')
