load data 
insert into table checkins 
fields terminated by "," 
trailing nullcols
(cust_id, bar_id, start_date TIMESTAMP 'YYYY-MM-DD-HH24-MI-SS', end_date TIMESTAMP 'YYYY-MM-DD-HH24-MI-SS', duration, duration_unit, price)
