DROP TABLE bars CASCADE CONSTRAINTS;
CREATE TABLE bars (
	bar_id INTEGER PRIMARY KEY,
	username VARCHAR2(30) NOT NULL,
	hashed_password VARCHAR2(100) NOT NULL,
	name VARCHAR2 (100),
	street_address VARCHAR2 (100),
	city VARCHAR2 (30),
	state VARCHAR2 (20),
	zip VARCHAR2 (6),
	phone_number VARCHAR2(15),
	email VARCHAR(30),
	website_url VARCHAR2(100),
	profile_picture INTEGER NOT NULL
);
DROP SEQUENCE bar_id_seq;
CREATE SEQUENCE bar_id_seq START WITH 1;

CREATE OR REPLACE TRIGGER bar_id_trig 
BEFORE INSERT ON bars
FOR EACH ROW

BEGIN
  SELECT bar_id_seq.NEXTVAL
  INTO   :new.bar_id
  FROM   dual;
END;
/
exit;	
