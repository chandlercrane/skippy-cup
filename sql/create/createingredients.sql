DROP TABLE ingredients CASCADE CONSTRAINTS;
CREATE TABLE ingredients (
	ingredient_id INTEGER PRIMARY KEY,
	name VARCHAR2(100)
);
DROP SEQUENCE ingredient_id_seq;
CREATE SEQUENCE ingredient_id_seq START WITH 1;

CREATE OR REPLACE TRIGGER ingredient_id_trig 
BEFORE INSERT ON ingredients
FOR EACH ROW

BEGIN
  SELECT ingredient_id_seq.NEXTVAL
  INTO   :new.ingredient_id
  FROM   dual;
END;
/
exit;
