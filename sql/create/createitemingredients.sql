DROP TABLE item_ingredients;
CREATE TABLE item_ingredients (
	item_id INTEGER,
	ingredient_id INTEGER,
	CONSTRAINT item_fk FOREIGN KEY (item_id) REFERENCES items(item_id) ON DELETE CASCADE,
	CONSTRAINT ingredient_fk FOREIGN KEY (ingredient_id) REFERENCES ingredients(ingredient_id) ON DELETE CASCADE,
	PRIMARY KEY (item_id, ingredient_id)
);
exit;
