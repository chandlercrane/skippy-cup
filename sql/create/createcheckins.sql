DROP TABLE checkins CASCADE CONSTRAINTS;
CREATE TABLE checkins (
	cust_id INTEGER,
	bar_id INTEGER,
	start_date TIMESTAMP NOT NULL,
	end_date TIMESTAMP NOT NULL,
	price NUMBER (*, 2) NOT NULL,
	duration INTEGER NOT NULL,
	duration_unit VARCHAR2 (15),
	FOREIGN KEY (cust_id) REFERENCES customers(cust_id) ON DELETE CASCADE,
	FOREIGN KEY (bar_id) REFERENCES bars(bar_id) ON DELETE CASCADE,
	PRIMARY KEY (cust_id, bar_id, start_date)
);
exit;
