DROP TABLE purchases CASCADE CONSTRAINTS;
CREATE TABLE purchases (
	purchase_id INTEGER PRIMARY KEY,
	cust_id INTEGER NOT NULL,
	bar_id INTEGER NOT NULL,
	order_time TIMESTAMP,
	final_cost NUMBER (*, 2),
	final_tip NUMBER (*, 2),
	purchase_method VARCHAR2(10),
	customer_ordered SMALLINT NOT NULL,		
	drink_prepared SMALLINT NOT NULL,
	drink_picked_up SMALLINT NOT NULL
);
DROP SEQUENCE purchase_id_seq;
CREATE SEQUENCE purchase_id_seq START WITH 1;

CREATE OR REPLACE TRIGGER purchase_id_trig 
BEFORE INSERT ON purchases
FOR EACH ROW

BEGIN
  SELECT purchase_id_seq.NEXTVAL
  INTO   :new.purchase_id
  FROM   dual;
END;
/
exit;
