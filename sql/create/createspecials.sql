DROP TABLE specials CASCADE CONSTRAINTS;
CREATE TABLE specials (
	special_id INTEGER PRIMARY KEY,
	drink_id INTEGER,
	special_price NUMBER (*,2) NOT NULL,
	special_note VARCHAR2 (100),
	start_date TIMESTAMP,
	end_date TIMESTAMP,
	day INTEGER,
	start_time TIMESTAMP,
	end_time TIMESTAMP,
	FOREIGN KEY (drink_id) REFERENCES drinks(drink_id) ON DELETE CASCADE
);
DROP SEQUENCE special_id_seq;
CREATE SEQUENCE special_id_seq START WITH 1;

CREATE OR REPLACE TRIGGER special_id_trig 
BEFORE INSERT ON specials
FOR EACH ROW

BEGIN
  SELECT special_id_seq.NEXTVAL
  INTO   :new.special_id
  FROM   dual;
END;
/
exit;
