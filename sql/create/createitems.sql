DROP TABLE items CASCADE CONSTRAINTS;
CREATE TABLE items (
	item_id INTEGER PRIMARY KEY,
	name VARCHAR2 (50),
	brand VARCHAR2 (20),
	drink_type VARCHAR2 (15),
	popular INTEGER	NOT NULL,
	profile_picture INTEGER NOT NULL
);
DROP SEQUENCE item_id_seq;
CREATE SEQUENCE item_id_seq START WITH 1;

CREATE OR REPLACE TRIGGER item_id_trig 
BEFORE INSERT ON items
FOR EACH ROW

BEGIN
  SELECT item_id_seq.NEXTVAL
  INTO   :new.item_id
  FROM   dual;
END;
/
exit;
