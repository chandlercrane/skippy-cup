DROP TABLE drinks CASCADE CONSTRAINTS;
CREATE TABLE drinks (
	drink_id INTEGER PRIMARY KEY,
	bar_id INTEGER,
	item_id INTEGER,
	price NUMBER(*,2),
	size_ounces INTEGER,
	container VARCHAR2 (15),
	note VARCHAR2(100),
	FOREIGN KEY (bar_id) REFERENCES bars(bar_id) ON DELETE CASCADE,
	FOREIGN KEY (item_id) REFERENCES items(item_id) ON DELETE CASCADE
);
DROP SEQUENCE drink_id_seq;
CREATE SEQUENCE drink_id_seq START WITH 1;

CREATE OR REPLACE TRIGGER drink_id_trig 
BEFORE INSERT ON drinks
FOR EACH ROW

BEGIN
  SELECT drink_id_seq.NEXTVAL
  INTO   :new.drink_id
  FROM   dual;
END;
/
exit;
