DROP TABLE purchase_drinks CASCADE CONSTRAINTS;
CREATE TABLE purchase_drinks (
	purchase_id INTEGER,
	drink_id INTEGER,
	note VARCHAR2(256),
	quantity SMALLINT,
	FOREIGN KEY (purchase_id) REFERENCES purchases(purchase_id) ON DELETE CASCADE,
	FOREIGN KEY (drink_id) REFERENCES drinks(drink_id) ON DELETE CASCADE,
	PRIMARY KEY (purchase_id, drink_id)
);
exit;
