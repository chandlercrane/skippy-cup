DROP TABLE customers CASCADE CONSTRAINTS;
CREATE TABLE customers (
	cust_id INTEGER PRIMARY KEY,
	username VARCHAR2 (30) NOT NULL,
	hashed_password VARCHAR2 (100) NOT NULL,
	first_name VARCHAR2 (30) NOT NULL,
	last_name VARCHAR2 (30) NOT NULL,
	profile_picture SMALLINT NOT NULL
);

DROP SEQUENCE cust_id_seq;
CREATE SEQUENCE cust_id_seq START WITH 1;

CREATE OR REPLACE TRIGGER cust_id_trig 
BEFORE INSERT ON customers
FOR EACH ROW

BEGIN
  SELECT cust_id_seq.NEXTVAL
  INTO   :new.cust_id
  FROM   dual;
END;
/
exit;	
