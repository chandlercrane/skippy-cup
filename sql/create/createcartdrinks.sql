DROP TABLE cart_drinks CASCADE CONSTRAINTS;
CREATE TABLE cart_drinks (
	cart_id INTEGER,
	drink_id INTEGER,
	note VARCHAR2(256),
	quantity SMALLINT,
	FOREIGN KEY (cart_id) REFERENCES carts(cart_id) ON DELETE CASCADE,
	FOREIGN KEY (drink_id) REFERENCES drinks(drink_id) ON DELETE CASCADE,
	PRIMARY KEY (cart_id, drink_id)
);
exit;
