#!/bin/bash

tables=(customers checkins bars items purchases ingredients drinks specials itemingredients cartdrinks carts purchasedrinks)

for i in "${tables[@]}"
do
	sqlplus -S $DB_USER/$DB_PASSWD @create/create${i} 
	sqlldr guest/guest log=log/${i}.log data=csv/${i}.csv skip=1 control=ctl/${i}.ctl bad=bad/${i}.bad	
done
