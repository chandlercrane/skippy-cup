$( document ).ready(function() {
	$('#contentContainer').removeClass('container');
	$('.create-account-link').click(showCreateAccount);
	$('.login-link').click(showLogin);
});

function showCreateAccount() { 
	$("#invalid-login-alert").css("display", "none");
	$('#log-in').css('display', 'none');
	$('#create-account').css('display', 'flex');
}

function showLogin() {
 	$("#invalid-create-account-alert").css("display", "none");
	$('#log-in').css('display', 'flex');
	$('#create-account').css('display', 'none');
}


async function logIn() {
	$("#invalid-login-alert").css("display", "none");
	$('#login-spinner').css("display", "block");
	var username = $('#username').val();
	var password = $('#password').val(); 
	var response = await generalXMLHttp("POST", hostname_and_port() + "/login", true, JSON.stringify({'username': username, 'password': password}), null, null, generalXMLErrorCallback, null);
	var parsedResponse = JSON.parse(response);
	$('#login-spinner').css("display", "none");
	if (parsedResponse['success'] == 'yes') { 
		if (parsedResponse['type'] == 'bar') window.location.href = hostname_and_port() + "/bar_dashboard";
		else if (parsedResponse['type'] == 'checkin')  window.location.href = hostname_and_port() + "/checkin";
		else window.location.href = hostname_and_port() + "/customer";
	} else  $("#invalid-login-alert").css("display", "block");
}

async function createAccount() { 
	$("#invalid-create-account-alert").css("display", "none");
	$("#create-account-spinner").css("display","block");	
	var first_name = $('#first-name').val();
	var last_name = $('#last-name').val();
	var email = $('#email').val();
	var username = $('#username-create').val();
	var password = $('#password-create').val();
	var response = await generalXMLHttp("POST", hostname_and_port() + "/signup", true, JSON.stringify({'username': username, 'password': password, 'first_name': first_name, 'last_name': last_name, 'email': email}), null, null, generalXMLErrorCallback, null);	
	var parsedResponse = JSON.parse(response);
	console.log("response: " + response);
	if (parsedResponse["success"] == "yes") window.location.href = hostname_and_port() + "/checkin";
	else {
		$("#invalid-create-account-message").html(parsedResponse["error"]);	
		$("#invalid-create-account-alert").css("display", "block");
	}
	$("#create-account-spinner").css("display","none");
}
