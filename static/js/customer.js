
function setupDropdown(){
	const allBars = document.getElementsByClassName("bar-item")
	const checkInBarName = document.getElementById('checkInBarName')
	const checkInBars = document.getElementsByClassName('checkInBar')
	const checkInBarAddress = document.getElementById('checkInBarAddress')
	for(bar of allBars){
		bar.addEventListener('click', e => {
			b = JSON.parse(e.target.getAttribute('data-bar'))
			checkInBarName.innerText = b.name
			console.log(b)
			for(z of checkInBars){
				z.value = e.target.getAttribute('data-bar')
			}
			checkInBarAddress.innerText = b.street_address + ', ' + b.city + ', ' + b.state
		})
	}
}

function cleanJSONString(s){
	s = s.replace(/\\n/g, "\\n")
		.replace(/\\'/g, "\\'")
		.replace(/\\"/g, '\\"')
		.replace(/\\&/g, "\\&")
		.replace(/\\r/g, "\\r")
		.replace(/\\t/g, "\\t")
		.replace(/\\b/g, "\\b")
		.replace(/\\f/g, "\\f")
	// remove non-printable and other non-valid JSON chars
	s = s.replace(/[\u0000-\u0019]+/g,"")
	return s
}

function dateAdd(date, interval, units) {
  if(!(date instanceof Date))
    return undefined;
  var ret = new Date(date); //don't change original date
  var checkRollover = function() { if(ret.getDate() != date.getDate()) ret.setDate(0);};
  switch(String(interval).toLowerCase()) {
    case 'year'   :  ret.setFullYear(ret.getFullYear() + units); checkRollover();  break;
    case 'quarter':  ret.setMonth(ret.getMonth() + 3*units); checkRollover();  break;
    case 'month'  :  ret.setMonth(ret.getMonth() + units); checkRollover();  break;
    case 'week'   :  ret.setDate(ret.getDate() + 7*units);  break;
    case 'day'    :  ret.setDate(ret.getDate() + units);  break;
    case 'hour'   :  ret.setTime(ret.getTime() + units*3600000);  break;
    case 'minute' :  ret.setTime(ret.getTime() + units*60000);  break;
    case 'second' :  ret.setTime(ret.getTime() + units*1000);  break;
    default       :  ret = undefined;  break;
  }
  return ret;
}

async function buyPlan(type){
	data = {}
	prices = [7, 13, 16]
	date = new Date();
	data['bar_id'] = checkInBarID.innerText
	data['price'] = prices[type]
	data['start_date'] = date
	data['allbars'] = false
	if(type == 1){
		data['allbars'] = true
	}
	if(type == 0 || type == 1){
		end_date = new Date()
		end_date.setDate(end_date.getDate() + 1)
		data['end_date'] = end_date
		data['duration'] = 1
		data['duration_unit'] = 'day'
	}
	else if(type == 2){
		end_date = new Date()
		end_date.setDate(end_date.getDate() + 7)
		data['end_date'] = end_date
		data['duration'] = 1
		data['duration_unit'] = 'week'
	}
	console.log(data)
	var response = await generalXMLHttp("POST", "http://3.88.92.76:8100/create-checkin", true, JSON.stringify(data), null, null, generalXMLErrorCallback, null);

}

function highlightNav(){
	const selected = document.getElementById('selectedNav')
	route = selected.getAttribute('value')
	console.log(route)
	if(route == '/checkout'){
		const checkout = document.getElementById('checkout')
		checkout.style.color = 'white'
	}
	else if(route == '/customer'){
		const menu = document.getElementById('menu-nav')
		menu.style.color = 'white'
	}
	else if(route == '/checkin'){
		const checkin = document.getElementById('checkin-nav')
		checkin.style.color = 'white'
	}
	else if(route == '/orders'){
		const orders = document.getElementById('orders-nav')
		orders.style.color = 'white'
	}
	else if(route == '/settings'){
		const settings = document.getElementById('settings-nav')
		settings.style.color = 'white'
	}
}


function setupCheckins(){
	setupDropdown()
}

function addDrinkToCart(item_id, cart_id){
	didl = "didLabel" + item_id
	var didLabel = document.getElementById(didl)
	drink_id = didLabel.innerHTML
	nl = "noteLabel" + item_id
	var noteLabel = document.getElementById(nl)
	note = noteLabel.value
	// add drink to cart
	console.log("adding drink(" + drink_id + ") with note -- " + note + " to cart(" + cart_id + ")")
}

var allBeers = document.getElementsByClassName('Beer')
var allCocktails = document.getElementsByClassName('Cocktail')
var allShots = document.getElementsByClassName('Shot')
var allDrinks = document.getElementsByClassName('Drink')

function changeView(param){
	var beerViewButton = document.getElementById('Beer')
	var cocktailViewButton = document.getElementById('Cocktail')
	var shotViewButton = document.getElementById('Shot')

	setupViews(param)
	if(param == 'Beer'){
		beerViewButton.classList.add('selected')
		cocktailViewButton.classList.remove('selected')
		shotViewButton.classList.remove('selected')
	}
	if(param == 'Cocktail'){
		beerViewButton.classList.remove('selected')
		cocktailViewButton.classList.add('selected')
		shotViewButton.classList.remove('selected')
	}
	if(param == 'Shot'){
		beerViewButton.classList.remove('selected')
		cocktailViewButton.classList.remove('selected')
		shotViewButton.classList.add('selected')
	}
}

function setupViews(param){
	for(i = 0; i < allDrinks.length; i++){
		allDrinks[i].style = "display:none"
	}
	if(param == 'Beer'){
		for(i = 0; i < allBeers.length; i++){
			allBeers[i].style = "visibility:visible"
		}
	}
	if(param == 'Cocktail'){
		for(i = 0; i < allCocktails.length; i++){
			allCocktails[i].style = "visibility:visible"
		}
	}
	if(param == 'Shot'){
		for(i = 0; i < allShots.length; i++){
			allShots[i].style = "visibility:visible"
		}
	}
}

function start(){
	setupViews('Beer')
	setupCheckins()
	highlightNav()
	//setupChangeBars()
}

document.addEventListener('DOMContentLoaded', function() {
	start()
}, false);

function setupChangeBars(){
	const bars = document.getElementsByClassName('bar-pick')
	for(bar of bars){
		bar.addEventListener('click', e => {
			b = JSON.parse(e.target.getAttribute('data-bar'))
			console.log(b)
			checkInBarName.innerText = b.name
			const bartitle = document.getElementById('bar-name')
			bartitle.innerText = bar.name
			console.log(b)
			checkInBarAddress.innerText = b.street_address + ', ' + b.city + ', ' + b.state
		})
	}
	const menus = document.getElementsByClassName('menu-row')
	for(i = 0; i < menus.length; i++){
		console.log(menus[i])
		menus[i].style = "display: none"
	}
	if(menus.length > 0){
		menus[0].style = "display:none"
	}
}

function switchBar(bid, name){
	const menu = document.getElementsByClassName('menu-'+bid)
	const bartitle = document.getElementById('bar-name')
	console.log(menu[0])
	menu[0].style = "display:flex"
	bartitle.innerText = name
	
}

function changeBar(menu){
	bar_id = menu['bar_id']
	bar_name = menu['bar_name']
	switchBar(bar_id, bar_name)	
}

function changeStyle(drink, style, bar_id){

	item_id = drink['item_id']
	drink_id = style['drink_id']
	size = style['size']
	container = style['container']
	price = style['pricestr']
	note = style['note']	
	
	var add_drink_drink = document.getElementById('add-drink-drink-' + item_id + '-' + bar_id)
	add_drink_drink.value = drink_id

	plID = "priceLabel" + item_id
	var priceLabel = document.getElementById(plID)
	priceLabel.innerText = "$" + price
	
	scID = "styleLabel" + item_id
	var styleLabel = document.getElementById(scID)
	styleLabel.innerText = size + 'oz. ' + container

	nID = "noteLabelBar" + item_id
	var noteLabel = document.getElementById(nID)
	noteLabel.innerText = note
	
	didLabel = "didLabel" + item_id
	var didLabel = document.getElementById(didLabel)
	didLabel.innerText = drink_id
}



