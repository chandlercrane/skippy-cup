var timeoutDict = {}

function settingsUpdateCallback(el) { 
	console.log("we successfully updated " + el.id + "!");
	$(el).removeClass("is-invalid");
	$(el).parent().parent().children('.invalid-feedback').css('display', 'none');
	$(el).addClass("is-valid");
	$(el).parent().parent().children('.valid-feedback').css('display', 'block');
	setTimeout(function () { 
		$(el).removeClass("is-valid");
		$(el).parent().parent().children('.valid-feedback').css('display', 'none'); 
	}, 2000);
}


function newTimeout(el, bid) { 
	return setTimeout(async function () {
		var validated;
		switch (el.type) { 
			case 'email':
				validated = email_validation(el.value);	
				break;
			case 'tel':
				validated = phone_validation(el.value);
				break;
			case 'url':
				validated = url_validation(el.value);
				break;
			case 'text':
				if (el.id == 'state') validated = state_validation(el.value); 	
				else if (el.id == 'zip') validated = zip_validation(el.value);
				else if (el.id == 'username') validated = await username_validation(el.value, bid);
				else validated = true; 
				break;
			default:
				validated = true;			
		}
		if (!validated) {
 			$(el).removeClass("is-valid");
			$(el).parent().parent().children('.valid-feedback').css('display', 'none');
			$(el).parent().parent().children('.invalid-feedback').css('display', 'block');
			$(el).addClass("is-invalid");
		} else if (el.id != "username") { 
			var response = generalXMLHttp("PUT", hostname_and_port() + "/settings_update/" + bid, true, JSON.stringify({'column': el.id, 'value': el.value}), settingsUpdateCallback, el, generalXMLErrorCallback, null);
			console.log("received response: " + response);
		} else { // username is validated
			settingsUpdateCallback(el);	
		}
		delete timeoutDict[el.id];
	}, 1000);
}

function validate_input(el, bid) {
	if (el.id in timeoutDict) { 
		clearTimeout(timeoutDict[el.id]);			
	}
	timeoutDict[el.id] = newTimeout(el, bid);
}

function changePasswordError(err) { 
	$('#incorrectPasswordChange').html(err);
	$('#incorrectPasswordChange').css('display', 'block');
}

function passwordUpdateCallback() { 

}

async function updatePassword(obj) { 
	var bid = obj.data.bid;	
	// stage 1 : client errors
	var letterNumber = /^[0-9a-zA-Z]+$/
	var pw0 = $('#currentPassword').val();
	var pw1 = $('#newPassword1').val();
	var pw2 = $('#newPassword2').val();
	if (pw1 !== pw2)  changePasswordError('New passwords do not match!');
	else if (pw1.length < 8 || pw1.match(letterNumber) == null) changePasswordError('New password must be at least 8 characters long and have both letters and numbers.');

	// stage 2 : server errors
	else {
		$('#incorrectPasswordChange').html('Saving...');
		$('#incorrectPasswordChange').css('display', 'block');
		var response = await generalXMLHttp("PUT", hostname_and_port() + "/password_update/" + bid, true, JSON.stringify({"old_password": pw0, "new_password": pw1}), passwordUpdateCallback, bid, generalXMLErrorCallback, null);
		var parsedResponse = JSON.parse(response);
		if (parsedResponse["success"] == "yes") $('#theModal').modal('hide');
		else changePasswordError('Incorrect current password. Try again.');
	}
		
}

function changePassword(bid) {
	console.log('here');
	console.log(bid); 
	populateModal({
		'title': 'Change Password',
		'body': '<form><div class="container"><div class="alert alert-warning" id="incorrectPasswordChange"></div><div class="row"><div class="col-12"><div class="form-group mb-3"><label for="currentPassword">Enter Current Password:</label><div class="input-group"><input type="password" class="form-control"  aria-label="Current Password" aria-describedby="basic-addon1" id="currentPassword"></div></div></div><div class="col-12"><div class="form-group mb-3"><label for="newPassword1">Enter New Password:</label><div class="input-group"><input type="password" class="form-control"  aria-label="New Password 1" aria-describedby="basic-addon1" id="newPassword1"></div></div></div><div class="col-12"><div class="form-group mb-3"><label for="newPassword2">Re-enter New Password:</label><div class="input-group"><input type="password" class="form-control"  aria-label="New Password 2" aria-describedby="basic-addon1" id="newPassword2"></div></div></div></div></div></form',
		'firstButton': 'Cancel',
		'secondButton': 'Save Changes',
		'confirmHandler': updatePassword,
		'confirmHandlerArg': {"bid":bid}
	});
}

function fileToUploadChosen(file, bid) {
  console.log("file to upload chosen");
  if (!file) return;
  const reader = new FileReader();

  reader.onprogress = function (data) { 
	if (data.lengthComputable) { 
		var progress = parseInt( ((data.loaded / data.total) * 100), 10 );
		console.log(progress + "% loaded!");
	}
  }

  reader.onload = function(e) {
	$('#barProfilePicture').removeAttr('src');
	$('#barProfilePicture').attr('src', e.target.result);	
  };  
  reader.readAsDataURL(file);
}

$(function() {
    $('#barProfilePictureInput').change( async function() {
        var form_data = new FormData($('#barProfilePictureForm')[0]);
		for (var pair of form_data.entries()) { 
			if (!(pair[1].name)) return; // don't update if no file added! 	
		}
		var response = await generalXMLHttp("POST", hostname_and_port() + "/bar_profile_picture_update/1", true, form_data, null, null, generalXMLErrorCallback, null, true);
		console.log(response);	
        /*$.ajax({
            type: 'POST',
            url: '/bar_profile_picture_update/1',   // fix!
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            success: function(data) { 
            	//var response = JSON.parse(data);
			},
        });*/
    });
});

window.onload = function() { 
	document.getElementById("barProfilePicture").src += "?" + (new Date().getTime());
}
