var menu_item_information = {};
var popular_menu_items;
var item_id;

async function changeMenuOrder(v) { 
	console.log(v);
	var response = await generalXMLHttp("POST", hostname_and_port() + "/update_menu_sequence", true, JSON.stringify({"sorting_method": v}), null, null, generalXMLErrorCallback, null);
	$('.menu-item-column').not(':first').remove();
	$(response).appendTo('#menu-item-row');
}

async function grabExistingMenuItem(drink_id) { 
	var response = await generalXMLHttp("GET", hostname_and_port() + "/get_existing_item_data/" + drink_id, true, null, null, null, generalXMLErrorCallback, null);
	menu_item_information = JSON.parse(response);
	$('.modal-footer').css('display', 'flex');
	$('#theModalButton2').css('opacity', '1');
	$('#theModalButton2').prop('disabled', false);
	if (menu_item_information["item_data"]["POPULAR"]) openMenuItemDrink(); 
	else openMenuItemLanding();
	console.log(menu_item_information)
}

function undoItemSelection() { 
	$('.inputTopper').css('display', 'none');
	$('#theModalButton2').css('opacity', '0.5');
	$('#theModalButton2').prop('disabled', true);
	$('#browseItemsInput').prop('disabled', false);
	$('#browseItemsInput').val('');
}


function openMenuItemLanding() {
	menu_item_information["item_exists"] = false;
	menu_item_information["drink_exists"] = false;
	populateModal({'title': 'New Menu Item', 'body': '<h5 class="text-left modal-block" id="chooseHeader"> <button class="orderButton prepareNotifyButton" id="backButton"><i class="fas fa-arrow-left"></i></button> Back</h5><div id="newMenuContent"><div class="flexStartFlexboxHorizontal modal-block"><button class="modalButton orderButton dropButton createMenuButton" onclick="createMenuItemFromScratch()" disabled style="opacity:0.5"><i class="fas fa-file"></i> Create From Scratch</button><button class="modalButton orderButton prepareNotifyButton createMenuButton" onclick="createMenuItemBrowse()"><i class="fas fa-search"></i> Browse Popular Items</button></div><div class="alert alert-secondary text-left modal-block"><i class="fas fa-info-circle"></i> Note: Create from scratch only if this menu item is unique to your bar. Browse popular items if even one of your customers will have heard of this drink. Chances are we\'ve got it here.</div></div><div id="browseItemsForm"><div class="input-group modal-block autocomplete-input"><button class="inputTopper barInformationButton"><i class="fas fa-undo"></i></button><div id="autocomplete-div"></div><div class="input-group-prepend"><span class="input-group-text" id="basic-addon1"><i class="fas fa-search"></i></span></div><input type="text" class="form-control" placeholder="Browse popular items" aria-label="Browse popular items" aria-describedby="basic-addon1" id="browseItemsInput" onkeyup="autocompleteKeyUp()"></div> ','firstButton': 'Cancel', 'secondButton': 'Continue', 'confirmHandler': null, 'confirmHandlerArg': {'new': true}}); 
	$('.modal-footer').css('display', 'none');
}

function createMenuItemFromScratch() { 
	/* TODO */
	//$('#newMenuContent').css('display', 'none');
}

async function createMenuItemBrowse() {
	$('#newMenuContent').css('display','none');
	$('#browseItemsForm').css('display','block');
	$('#theModalButton2').css('opacity', '0.5');
	$('#theModalButton2').prop('disabled', true); 
	$('.modal-footer').css('display', 'flex');
	var response = await generalXMLHttp("GET", hostname_and_port() + "/get_all_items", true, null, null, null, generalXMLErrorCallback, null);
	popular_menu_items = JSON.parse(response)['item_data'].sort((a,b) => (a["NAME"] > b["NAME"]) ? 1 : -1 );	
}

function escapeRegExp(text) {
  return text.replace(/[-[\]{}()*+?.,\\^$|#\\s]/g, '\\$&');
}

function autocompleteKeyUp() { 
	var v = $('#browseItemsInput').val();
	console.log(v);
	if (!v) { 
		$('#autocomplete-div').css('display', 'none'); 
		return;
	}
	var regex_v = escapeRegExp(v);
	var html = [];
	var current_regex;
	for (var i = 0; i < popular_menu_items.length; i++) { 
		current_regex = new RegExp(".*" + regex_v + ".*", 'i');
		if (popular_menu_items[i]["NAME"].match(current_regex) != null) {
			console.log('match');
			html.push('<div class="autocomplete-item" onclick="chooseAutoCompleteItem(\'' + popular_menu_items[i]["ITEM_ID"] + '\', \'' + popular_menu_items[i]["NAME"] + '\');">');
			if (popular_menu_items[i]["PROFILE_PICTURE"] == 1) html.push('<img class="donutPicture" src="/static/data/items/' + popular_menu_items[i]["ITEM_ID"] + '.jpg"> ')
			html.push(popular_menu_items[i]["NAME"] + '</div>') 
			html.push('</div>');	
		}
	}
	$('#autocomplete-div').html(html.join(''));
	$('#autocomplete-div').css('display', 'block');
}

function chooseAutoCompleteItem(id, name) { 
	$('#autocomplete-div').css('display', 'none');
	$('#browseItemsInput').val(name);	
	$('.inputTopper').css('display', 'block');
	$('.inputTopper').click(undoItemSelection);
	$('#browseItemsInput').prop('disabled', true);
	$('#theModalButton2').prop('disabled', false);
	$('#theModalButton2').off('click');
	menu_item_information['item_exists'] = true;
	menu_item_information['item_data'] = {'ITEM_ID': id, 'NAME': name, 'POPULAR': true}
	$('#theModalButton2').click(openMenuItemDrink);
	$('#theModalButton2').css('opacity', '1');
}


function openMenuItemItem() {

}

function openMenuItemDrink() { 	
	populateModal({'title': menu_item_information['item_data']["NAME"], 'body': '<div class="alert alert-warning" id="invalid-drink-alert">Invalid drink information. Fix highlighted fields.</div><form><div class="form-group row"><label for="price" class="col-3 col-form-label">Price</label><div class="col-9"><div class="input-group"><div class="input-group-prepend"><div class="input-group-text"><i class="fas fa-dollar-sign"></i></div></div><input type="text" class="form-control" id="price"></div><small id="price-help-block" class="form-text text-muted"><span class="mandatory">Required.</span> Non-special retail drink price.</small></div></div><div class="form-group row"><div class="col-3 col-form-label">Size (oz)</div><div class="col-3 col-form-label"><div class="form-check"><input class="form-check-input" type="checkbox" id="size-checkbox" onchange="checkSize()";><label class="form-check-label" for="size">N/A</label></div></div><div class="col-6"><div class="input-group"><input type="text" class="form-control" id="size"><div class="input-group-append"><div class="input-group-text">oz</div></div></div></div></div><div class="form-group row"><label for="container" class="col-3 col-form-label">Container</label><div class="col-9"><div class="input-group"><input type="text" class="form-control" id="container"></div><small id="container-help-block" class="form-text text-muted"><span class="mandatory">Optional.</span> Ex. glass, bottle, pitcher, etc.</small></div></div><div class="form-group row"><label for="note" class="col-3 col-form-label">Menu Note</label><div class="col-9"><div class="input-group"><textarea class="form-control" id="note" rows="3"></textarea></div><small id="container-help-block" class="form-text text-muted"><span class="mandatory">Optional.</span> Tell your customers about this item.</small></div></div></form>', 'firstButton': 'Cancel', 'secondButton': 'Continue', 'confirmHandler': validate_drinks, 'confirmHandlerArg': null});
	if (menu_item_information['drink_exists']) { 
		$('#price').val(menu_item_information['drink_data']['PRICE']);
		if (menu_item_information['drink_data']['SIZE_OUNCES']) $('#size').val(menu_item_information['drink_data']['SIZE_OUNCES'].toString());
		else {
			$('#size-checkbox').prop('checked', true);
			$('#size').val("0");	
			$('#size').prop('disabled', true);
		}
		if (menu_item_information['drink_data']['CONTAINER'] != 'N/A') $('#container').val(menu_item_information['drink_data']['CONTAINER']);
		$('#note').val(menu_item_information['drink_data']['NOTE']);
	}
}

function validate_drinks() { 
	var p = $('#price').val();
	var s = $('#size-checkbox').is(":checked") ? "0" : $('#size').val();
	var c = $('#container').val() != "" ? $('#container').val() : "N/A";
	var n = $('#note').val();
	var p_valid = price_validation(p);
	var s_valid = size_validation(s);
	if (!s_valid) $('#size').addClass('is-invalid');
	else $('#size').removeClass('is-invalid');
	if (!p_valid) $('#price').addClass('is-invalid');
	else $('#price').removeClass('is-invalid'); 
	
	if (s_valid && p_valid) {
		if (!menu_item_information["drink_exists"])menu_item_information["drink_data"] = {};	
		menu_item_information["drink_data"]['PRICE'] = p;
		menu_item_information["drink_data"]['SIZE_OUNCES'] = s;
		menu_item_information["drink_data"]['CONTAINER'] = c;
		menu_item_information["drink_data"]['NOTE'] = n;	
		openMenuItemSpecials();	
		console.log(menu_item_information);
	} else $('#invalid-drink-alert').css('display', 'block');	
}

function openMenuItemSpecials() { 
	populateModal({'title': menu_item_information['item_data']["NAME"], 'body': '<div class="alert alert-warning" id="invalid-special-alert">Invalid special information. Fix highlighted fields.</div><div class="alert alert-secondary text-left modal-block" id="no-specials-alert"><i class="fas fa-info-circle"></i> This menu item has no specials yet. Click the button below to create one!</div><div id="specials"></div><button id="addSpecialButton" onclick="addSpecial()" class="btn btn-secondary btn-block"><i class="fas fa-plus"></i> NEW SPECIAL</button>', 'firstButton': 'Cancel', 'secondButton': 'Save & Submit','confirmHandler': createFinalNewItem, 'confirmHandlerArg': null});
	if (menu_item_information['drink_exists']) { 
		for (var i = 0; i < menu_item_information['special_data'].length; i++) { 
			addSpecial(menu_item_information['special_data'][i]);	
		}
	} 

	if (!menu_item_information['drink_exists'] || menu_item_information['special_data'].length == 0) $('#no-specials-alert').css('display', 'block');
		
}

function addSpecial(s) {
	console.log(s);
	$('#no-specials-alert').css('display', 'none');
	var c = $('#specials').children().length;
	$('#specials').append('<div class="special" id="special-' + c + '"><div class="form-group row" id="row' + c + '.1"><label class="col-3 col-form-label special-label">Special</label><div class="col-6 centerFlexboxHorizontal alignCenter"><div class="form-check form-check-inline"><input class="form-check-input" type="radio" name="inlineRadioOptions' + c + '" id="temporary' + c + '" value="temporary"><label class="form-check-label" for="temporary' + c + '">Temporary</label></div><div class="form-check form-check-inline"><input class="form-check-input" type="radio" name="inlineRadioOptions' + c + '" id="recurring' + c + '" value="recurring" checked><label class="form-check-label" for="recurring">Recurring</label></div></div><div class="col-3"><button class="btn btn-secondary" onclick="deleteSpecial(' + c + ')">DELETE</button></div></div><div class="form-group row" id="row' + c + '.2"><label class="col-3 col-form-label">Weekday</label><div class="col-9 centerFlexboxHorizontal alignCenter"><select class="form-control" id="weekday' + c + '"><option value="" disabled selected>-</option><option value="0">Sunday</option><option value="1">Monday</option><option value="2">Tuesday</option><option value="3">Wednesday</option><option value="4">Thursday</option><option value="5">Friday</option><option value="6">Saturday</option></select></div></div><div class="form-group row" id="row' + c + '.3"><label class="col-3 col-form-label">Start</label><div class="col-9 centerFlexboxHorizontal"><div class="input-group start-end-group"><input type="text" class="form-control" id="start' + c + '"><small id="start-help-block" class="form-text text-muted"><span class="mandatory">Optional.</span> The time of night you want the special to begin.</small></div></div></div><div class="form-group row" id="row' + c + '.4"><label class="col-3 col-form-label">End</label><div class="col-9 centerFlexboxHorizontal"><div class="input-group start-end-group"><input type="text" class="form-control" id="end' + c + '"><small id="end-help-block" class="form-text text-muted"><span class="mandatory">Optional.</span> The time of night you want the special to end. Valid AM or PM.</small></div></div></div><div class="form-group row"><label for="special-price' + c + '" class="col-3 col-form-label">New Price</label><div class="col-9"><div class="input-group"><div class="input-group-prepend"><div class="input-group-text"><i class="fas fa-dollar-sign"></i></div></div><input type="text" class="form-control" id="special-price' + c + '"></div><small id="container-help-block" class="form-text text-muted"><span class="mandatory">Mandatory.</span> Special price of the item.</small></div></div><div class="form-group row"><label for="special-note' + c + '" class="col-3 col-form-label">Special Note</label><div class="col-9"><div class="input-group"><textarea class="form-control" id="special-note' + c + '" rows="3"></textarea></div><small id="container-help-block" class="form-text text-muted"><span class="mandatory">Optional.</span> Tell your customers about this special.</small></div></div></div><hr id="hr-' + c + '">');
	

	//$('#start').data("DateTimePicker").format('MMMM Do YYYY');
	if (s) { 
		if (s['DAY'] != null) {
			$('#recurring'+c).prop('checked', true); 
			$('#weekday' + c).val(s['DAY']);
			$('#start' + c).val(s['START_TIME']);
			$('#end' + c).val(s['END_TIME']);
			$('#start' + c).datetimepicker({ format: 'h:mm a', sideBySide: true});
			$('#end' + c).datetimepicker({ format: 'h:mm a', sideBySide: true});
		} else {
			$('#temporary'+c).prop('checked', true); 
			$('#weekday' + c).val('-');
			$('#weekday' + c).prop('disabled', true);
			$('#start' + c).val(s['START_DATE']);
			$('#end' + c).val(s['END_DATE']);
			$('#start' + c).datetimepicker({format:'MM/DD/YYYY h:mm a', sideBySide: true});
			$('#end' + c).datetimepicker({format: 'MM/DD/YYYY h:mm a', sideBySide: true});
		}
		$('#special-price' + c).val(s['SPECIAL_PRICE']);
		$('#special-note' + c).val(s['SPECIAL_NOTE']);
	} else { 
		    $('#start' + c).datetimepicker({ format: 'h:mm a', sideBySide: true});
            $('#end' + c).datetimepicker({ format: 'h:mm a', sideBySide: true})
	}

	$('input[type=radio][name=inlineRadioOptions' + c + ']').change(function() {
		if (this.value == "temporary"){
			$('#weekday' + c).prop('disabled', true);
			$('#weekday' + c).val('-');
			$('#start' + c).data("DateTimePicker").format('MM/DD/YYYY h:mm a');
			$('#end' + c).data("DateTimePicker").format('MM/DD/YYYY h:mm a');
			$('#start-help-block').html('<span class="mandatory">Required.</span> The moment that the special will take effect.');
			$('#end-help-block').html('<span class="mandatory">Required.</span> The moment that the special will expire.');
		} else {
			$('#weekday' + c).prop('disabled', false);
			$('#start' + c).data("DateTimePicker").format('h:mm a');
			$('#end' + c).data("DateTimePicker").format('h:mm a');
			$('#start-help-block').html('<span class="mandatory">Optional.</span> The time this item\'s weekly special will begin.');
			$('#end-help-block').html('<span class="mandatory">Optional.</span> The time this item\'s weekly special will end.');
		}		
	});
}


function deleteSpecial(idx) {
	$('#special-' + idx).remove();
	$('#hr-' + idx).remove();
	if ($('#specials').children().length == 0) $('#no-specials-alert').css('display', 'block');
}

async function createFinalNewItem() {
	var special_id;
	var validated = true;
	menu_item_information["special_data"] = []
	var special_dict;
	var specials = $('#specials').children().not('hr').each(function () { 
		special_dict = {};
		special_id = $(this).attr("id").slice(8);
		if ($('input[name=inlineRadioOptions' + special_id + ']:checked').val() == "recurring") { 
			if ($('#weekday' + special_id).find(":selected").text() == '-') { 
				$('#weekday' + special_id).addClass('is-invalid');
				validated = false;
			}
			special_dict["DAY"] = $('#weekday' + special_id).find(":selected").val();
			special_dict["START_TIME"] = $('#start' + special_id).val();
			special_dict["END_TIME"] = $('#end' + special_id).val(); 
		} else { 
			if ($('#start' + special_id).val() == '') {
				$('#start' + special_id).addClass('is-invalid');
				validated = false;
			}
			if ($('#end' + special_id).val() == '') { 
				$('#end' + special_id).addClass('is-invalid');
				validated = false;
			}

			special_dict["START_DATE"] = $('#start' + special_id).val();
			special_dict["END_DATE"] = $('#end' + special_id).val(); 
		}
		special_dict["SPECIAL_PRICE"] = $('#special-price' + special_id).val();
		special_dict["SPECIAL_NOTE"] = $('#special-note' + special_id).val();
		if (!special_dict["SPECIAL_PRICE"]) validated = false;	
		menu_item_information["special_data"].push(special_dict);
	});

	if (validated) {
		console.log(menu_item_information);
		$('#invalid-special-alert').css('display', 'none');
		var response = await generalXMLHttp("PUT", hostname_and_port() + "/update_menu_item_information", true, JSON.stringify(menu_item_information), newItemAdded, null, generalXMLErrorCallback, null);
		console.log(response);
		$('#theModal').modal("hide");	
	} else { 
		$('#invalid-special-alert').css('display', 'block');
	}
}


function newItemAdded() { 
	location.reload();
}

function checkSize() { 
	if ($('#size-checkbox').is(":checked")) { 
		$('#size').val("0");
		$('#size').removeClass('is-invalid');
		$('#size').prop('disabled', true);
	} else $('#size').prop('disabled', false);
}
