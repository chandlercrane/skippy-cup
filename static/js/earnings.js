Chart.defaults.global.defaultFontFamily = "Jost";
$(document).ready(function() { 
	getChartData("last_month");
});

async function getChartData(v) { 	
	var response = await generalXMLHttp("PUT",hostname_and_port() + "/get_earnings",true,JSON.stringify({'chart_range': v}), buildChart, null, generalXMLErrorCallback, null, null, true);		
}

var myChart;
function changeChartRange(v) {
	myChart.destroy()
	getChartData(v);	
}


function buildChartConfiguration(parsedChart) { 
		label_array = []
		border_array = []
		background_array = []
		data_array = []
		for (var i = 0; i < parsedChart.length; i++) { 
			label_array.push(parsedChart[i]["NIGHT_DATE"])
			data_array.push(parsedChart[i]["N_PURCHASES"])
			background_array.push("#FFDA85")
			border_array.push("#FEC652")
		}
	
		return {
			type: 'bar',
			data: {
				labels: label_array,
				datasets: [{
					data: data_array,
					backgroundColor: background_array,
					borderColor: border_array,
					borderWidth: 1
				}]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
				  			beginAtZero: true,
				  			callback: function(value) {if (value % 1 === 0) {return value;}}
						},
						scaleLabel: { 
							display: true, 
							labelString: 'Quantity',						}
					}]
				},
				title: { 
					display: true,
					fontSize: 20,	
					text: 'Purchases: Last Month'
				},
				legend: { 
					display: false 
				}
			}
		};
}


function buildChart(chartData) {
		var parsedTable = JSON.parse(chartData)["TABLE"];
		console.log(parsedTable);
		for (var key in parsedTable) { 
			if (!key.endsWith("FREQUENCY")) $("#" + key).html("$" + parsedTable[key]);
			else $("#" + key).html(parsedTable[key] + "%");
		}

		var parsedChart = JSON.parse(chartData)["GRAPH"];
		var config = buildChartConfiguration(parsedChart);
		var ctx = document.getElementById('myChart').getContext('2d');
		myChart = new Chart(ctx, config);
}
