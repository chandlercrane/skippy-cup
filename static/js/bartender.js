var refreshIndex = 0;
var interval;
var openPreparingOrder = 0, openServingOrder = 0;
var xhr = new XMLHttpRequest();
var time = 10;
var fulltime = 10;
var timerVisible = false;

function setTimeClock(t) {
	time = t;
	if (timerVisible) document.getElementById("timer").innerHTML = time; 
}

function toggleSwitchChange() { 
	if (document.getElementById("toggleSwitch").checked) { 
		document.getElementById("toggleSwitchComponent").innerHTML = '<div class="centerFlexboxHorizontal" id="refreshAutomatic"><i class="far fa-clock"></i> <div id="timer"></div> s</div>';
		countdown();
		restartAnimation();	
		document.getElementById("refreshIntervalRow").style.display = "flex";
		$('#secondOption').html("Refresh Timer");
		$('.settingsText').html("On");
		timerVisible = true;	
	} else { 
		document.getElementById("toggleSwitchComponent").innerHTML = '<div class="centerFlexboxHorizontal" id="refreshManual"><button class="barInformationButton" onclick="updatePurchases()"><i class="fas fa-hourglass-start"></i> REFRESH</button></div>';
		clearInterval(interval);
		$('#secondOption').html("Manual Refresh");
		$('.settingsText').html("Off");
		timerVisible = false;
		document.getElementById("refreshIntervalRow").style.display = "none";
	}
}

function restartAnimation() {
	setTimeClock(fulltime);	
	clearInterval(interval);
	countdown();
}



function addRefreshTime() {		 
	if (fulltime <= 115) fulltime += 5;
	restartAnimation();
	document.getElementById("refreshInterval").innerHTML = fulltime;
} 

function subtractRefreshTime() {
	if (fulltime >= 10) fulltime -= 5; 
	restartAnimation();
	document.getElementById("refreshInterval").innerHTML = fulltime; 
}

function assignPreparingOrder(n) {
	openPreparingOrder = n; 
}

function openOnePreparingOrder() {
  	 if (openPreparingOrder >0 ) $("#detailsDiv" + openPreparingOrder).collapse('toggle');	
}

// @param n: int: the order number to keep open on a refresh, if n > 0.
// if n == 0, then, this will be successfully assigned and  soon signal 
// the refresh function to assign a new order
// if n < 0, then this will automatically find a new order to open + assign.
function assignServingOrder(n) {
	openServingOrder = n;
}

function openOneServingOrder() { 
  	 if (openServingOrder > 0) $("#detailsDiv" + openServingOrder).collapse('show');	
}

function checkCardChildren() {
	var warningHTML = '<div class="alert alert-warning" id="drinksServedAlert" role="alert"><b>All prepared drinks have been served!</b> Stay tuned for more.</div>';
	var drinksOrdered = document.getElementById("drinksOrderedAccordion");
	var drinksPrepared = document.getElementById("drinksPreparedAccordion");
	if (drinksOrdered && !drinksOrdered.children.length) $('#drinksOrderedAccordion').html(warningHTML);
	if (drinksPrepared && !drinksPrepared.children.length) $('#drinksPreparedAccordion').html(warningHTML);
}

function confirmDroppedOrderCallback(id) {
	document.getElementById("card-" + id).remove();
    checkCardChildren();
    $('#theModal').modal('hide');	
}

function prepareOrderCallback(id) {
	openPreparingOrder = 0; 
	document.getElementById("card-" + id).remove();
    checkCardChildren();
	// attach the order to the end of 'served', temporarily	
}

function serveOrderCallback(id) {
	openServingOrder = 0; 
	document.getElementById("card-" + id).remove();
    checkCardChildren();
}


async function confirmDroppedOrder(obj) {
	var id = obj.data.id;
	var start = Date.now()
	var responseText = await generalXMLHttp("DELETE", hostname_and_port() + "/confirm_dropped_order/" + id, true, null, confirmDroppedOrderCallback, id, generalXMLErrorCallback, null);
	console.log("received a response from the server: " + responseText);
}

function updatePurchases() {  // readystate: 0-4; 0 = unsent, 1 = opened, 2 = headers, 3 = downloading, 4 = done
	if (document.getElementById("toggleSwitch").checked) setTimeClock(time);
	if (xhr.readyState > 0 && xhr.readyState < 4) xhr.abort(); 
	xhr.open("GET", hostname_and_port() + "/update_bartender", true); // asynchronous
	var start = Date.now();
	xhr.onreadystatechange = function (e) { 
		if (this.readyState == 4) { 
			if (this.status == 200) {
				console.log("in total, request took " + parseInt((Date.now() - start).toString()) / 1000 + "s");
				document.getElementById("theTwoQueues").innerHTML = xhr.responseText;
				addCollapseHandlers();
				openOnePreparingOrder();
				openOneServingOrder();
				$('.collapsing').addClass('noCollapseAnimation');
    		} else { 
				console.log(xhr.statusText);	
			}
		}
	}
	xhr.send();
}

function countdown() {
  var timer = document.getElementById("timer"); 
  document.getElementById("refreshInterval").innerHTML = fulltime;
  setTimeClock(time);
  timerVisible = true; 
  interval = setInterval(function() {	 
    if (time <= 0) { 
	  updatePurchases();
	  setTimeClock(fulltime);
    } else {
      setTimeClock(--time);  
    }    
  }, 1000);
}


function showRefreshSettings() {
	$('.modal-footer').css('display', 'none'); 
	populateModal({'title': 'Automatic Refresh Settings', 'body': '<div class="form-group row"><label class="col-5 col-form-label">Automatic Refresh</label><div class="col-6 flexStartFlexboxHorizontal alignCenter"><div class="form-check form-check-inline"><div class="flexStartFlexboxHorizontal" id="refreshSlider"><label class="switch"><input type="checkbox" id="toggleSwitch" onchange="toggleSwitchChange()"><span class="slider round"></span></label><div class="settingsText">On</div></div></div></div></div><div class="form-group row"><label class="col-5 col-form-label flexStartFlexboxHorizontal alignCenter" id="secondOption">Manual Refresh</label><div class="col-6 flexStartFlexboxHorizontal alignCenter"><div id="toggleSwitchComponent"></div></div></div><div class="form-group row" id="refreshIntervalRow"><label class="col-form-label col-5">Refresh Interval</label><div class="col-6 flexStartFlexboxHorizontal alignCenter"><button id="minusButton" class="timerChangeButton barInformationButton" onclick="subtractRefreshTime()"><i class="fas fa-minus"></i></button><div id="refreshInterval"></div><button class="timerChangeButton barInformationButton" onclick="addRefreshTime()"><i class="fas fa-plus"></i></button></div></div>', 'firstButton': 'Back', 'secondButton': 'Save', 'confirmHandler': null, 'confirmHandlerArg': null});

	if (!timerVisible) {
		$('#toggleSwitchComponent').html('<div id="refreshManual"><button class="barInformationButton" onclick="updatePurchases()"><i class="fas fa-hourglass-start"></i> REFRESH</button></div>'); 
	    $('#secondOption').html("Manual Refresh");
        $('.settingsText').html("Off");
        timerVisible = false;
        document.getElementById("refreshIntervalRow").style.display = "none";
	}else {
		$('#toggleSwitch').prop('checked', true);
		$('#toggleSwitchComponent').html('<div class="centerFlexboxHorizontal" id="refreshAutomatic"><i class="far fa-clock"></i> <div id="timer"></div> s</div>');
		document.getElementById("refreshIntervalRow").style.display = "flex";
        $('#secondOption').html("Refresh Timer");
        $('.settingsText').html("On");
		$('#refreshInterval').html(fulltime);
		setTimeClock(time);
	}
}
