$(document).ready(function(){
	$('#theModal').modal({
  		show: false
	});
	addCollapseHandlers();	
});

function addCollapseHandlers() {
	$('.collapse').collapse({
		toggle: false
	});		
	$('.expandPreparing').on('click', function () {
		console.log('ok...'); 
		var n = $(this).attr('id').slice(10);
		console.log('expandPreparing: ' + n);
		$('#detailsDiv' + n).collapse('toggle');
		assignPreparingOrder(n);
	});

	$('.expandServing').on('click', function () { 
		var n = $(this).attr('id').slice(10);
		console.log('expandServing: ' + n);
		$('#detailsDiv' + n).collapse('toggle');
		assignServingOrder(n);	
	});

	$('.collapse').on('show.bs.collapse', function () { 
		$('.collapse').removeClass('noCollapseAnimation');
	});
	$('.collapse').on('hide.bs.collapse', function () { 
		$('.collapse').removeClass('noCollapseAnimation');
	});

	$('.preparingCollapse').on('hidden.bs.collapse', function () {
		$('#expandIcon' + $(this).attr('id').slice(10)).html('&#9660;');
		console.log('preparingCollapse: ' + $(this).attr('id').slice(10));
		assignPreparingOrder(0);
	});
	$('.servingCollapse').on('hidden.bs.collapse', function () {
		$('#expandIcon' + $(this).attr('id').slice(10)).html('&#9660;');
		console.log('servingCollapse: ' + $(this).attr('id').slice(10));
		assignServingOrder(0);
	});
	$('.preparingCollapse').on('shown.bs.collapse', function () { 
		var id = $(this).attr('id').slice(10);
		console.log('.preparingCollapse: ' + id);
		$('#expandIcon' + id).html('&#9650;');
		assignPreparingOrder(id);
	});
	$('.servingCollapse').on('shown.bs.collapse', function () {
		var id = $(this).attr('id').slice(10); 
		console.log('.servingCollapse: ' + id);
		$('#expandIcon' + id).html('&#9650;');
		assignServingOrder(id);
	});

	$('.dropButton').not('#refresh-settings-button').on('click', function () { 
		$('.modal-footer').css('display', 'flex');
		populateModal({
			'title': 'Drop Order',
			'body': 'Are you sure you want to drop this order?<br><i class="fas fa-info-circle"></i> <em>Warning: such an action cannot be undone.</em>',
			'firstButton': '<i class="fas fa-times"></i> NO',
			'secondButton': '<i class="fas fa-check"></i> YES',
			'confirmHandler': confirmDroppedOrder,
			'confirmHandlerArg': {'id': $(this).attr('id').slice(10)}
		});
	});

	$('.prepareNotifyButton').on('click', async function () {
		var id = $(this).attr('id').slice(19);
		console.log('prepareNotifyButton: ' + id);
		var response = await generalXMLHttp("POST", hostname_and_port() + "/prepare_order/" + id, true, null, prepareOrderCallback, id, generalXMLErrorCallback, null); 
		console.log("prepare status: " + response);
	});

	$('.serveNotifyButton').on('click', async function () { 
		var id = $(this).attr('id').slice(17);
		console.log('serveNotifyButton: ' + id);
		var response = await generalXMLHttp("POST", hostname_and_port() + "/serve_order/" + id, true, null, serveOrderCallback, id, generalXMLErrorCallback, null);	
		console.log("serve status:" + response);
	});
}




