$(document).ready(function(){
    $('.closeFirstModal').click(closeFirstModal);
    $('.reopenFirstModal').click(reopenFirstModal);
    $('.confirmClosed').click(confirmClosed);	
	$('input[type=text]').change(removeInvalid);
});

function removeInvalid() { 
	console.log('mmhm');
	$(this).removeClass('is-invalid');
}

function closeFirstModal() {
	$('#theModal').modal('hide');
    $('#theWarningModal').modal('show');
}

function reopenFirstModal() { 
    $('#theWarningModal').modal('hide');
	$('#theModal').modal('show');
}

function confirmClosed() { 
    $('#theWarningModal').modal('hide');
    $('#theModal').modal('hide');
}


function changeText() { 
	document.getElementById("heading").innerHTML = "oooh yeah";
}

function populateModal(htmlDict) { // So that we can reuse this here one modal.
	$('#theModalTitle').html(htmlDict['title']);
	$('#theModalBody').html(htmlDict['body']);
	$('#theModalButton1').html(htmlDict['firstButton']);
	$('#theModalButton2').html(htmlDict['secondButton']);
	$('#theModalButton2').off('click'); // removes previous handler(s)
	if (htmlDict['confirmHandler']) {
		$('#theModalButton2').click(htmlDict['confirmHandlerArg'],htmlDict['confirmHandler']);
	}
	$('#theModal').modal('show');
}

function highlightNavLink() { 
	var scripts = document.getElementsByTagName("script");
	var filename = scripts[scripts.length - 1].src;
	var idx = filename.lastIndexOf("/");
	var nav_name = filename.slice(idx + 1, -3);
	var nav_items = document.getElementsByClassName("nav-item");
	var el = document.getElementById(nav_name);
	if (el) el.classList.add("active"); 
}

function changeURL(endpt) {  // for buttons
	window.location.href = hostname_and_port() + "/" + endpt;
}

window.addEventListener('load', highlightNavLink);

function generalXMLErrorCallback(s) {
	console.log(s);
}

function generalXMLHttp(request_type, url, async, reqbody, success_callback, success_params, error_callback, error_param, content_type_override, pass_response_to_callback) {
    var promise = new Promise(function (resolve, reject) {
        var generalXHR = new XMLHttpRequest();
        var start = Date.now();
		console.log(url);
        generalXHR.open(request_type, url, async);
		if (!content_type_override) generalXHR.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
		//else console.log("nope");
        generalXHR.onreadystatechange = function (e) {
            if (this.readyState == 4) {
                if (this.status == 200) {
                    if(success_callback) {
			if (!pass_response_to_callback) success_callback(success_params);
			else success_callback(generalXHR.responseText);
		    }
                    console.log("in total, request took " + parseInt((Date.now() - start).toString()) / 1000 + "s");
                    resolve(generalXHR.responseText);
                }
                else {
                    error_callback(generalXHR.statusText);
                }
            }
        }
        if (reqbody) generalXHR.send(reqbody);
		else generalXHR.send();
    });
    return promise;
}


function email_validation(txt) {
    if (!txt) return true; 
    return txt.match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/) != null;
}

function phone_validation(txt) { 
    if (!txt) return true;
    return txt.match(/^\(\d{3}\)-\d{3}-\d{4}$/) != null;    
}

async function username_validation(txt, bid) {
    if (!txt) return false; 
    var response = await generalXMLHttp("PUT", hostname_and_port() + "/bar_username_update_attempt/" + bid, true, JSON.stringify({'new_username':txt}), null, null, generalXMLErrorCallback, null);
    var responseJSON = JSON.parse(response);
    return responseJSON["success"] == "yes";
}

function url_validation(txt) { 
    if (!txt) return true;
    return txt.match(/((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/) != null;

}

function state_validation(txt) { 
    var states = ['AK', 'AL', 'AR', 'AS', 'AZ', 'CA', 'CO', 'CT', 'DC', 'DE', 'FL', 'GA', 'GU', 'HI', 'IA', 'ID', 'IL', 'IN', 'KS', 'KY', 'LA', 'MA', 'MD', 'ME', 'MI', 'MN', 'MO', 'MP', 'MS', 'MT', 'NC', 'ND', 'NE', 'NH', 'NJ', 'NM', 'NV', 'NY', 'OH', 'OK', 'OR', 'PA', 'PR', 'RI', 'SC', 'SD', 'TN', 'TX', 'UM', 'UT', 'VA', 'VI', 'VT', 'WA', 'WI', 'WV', 'WY']
    return txt.length == 2 && states.includes(txt);
}

function price_validation(txt) { 
	return txt.match(/^\$?(?!0.00)(([0-9]{1,3},([0-9]{3},)*)[0-9]{3}|[0-9]{1,3})(\.[0-9]{2})?$/) != null;
}

function size_validation(txt) { 
	return txt.match(/^\d+$/)	
}

function zip_validation(txt) {
    return txt.match(/(^\d{5}$)|(^\d{5}-\d{4}$)/) != null;
}

function hostname_and_port() {
	return window.location.protocol + "//" +  window.location.hostname + ":" + window.location.port;
} 
