from flask import render_template, url_for, redirect, request, session
from passlib.hash import sha256_crypt
import os
import cx_Oracle
from datetime import datetime, timedelta
import functools
import subprocess
import json
import time

## @param b: Boolean: True if you want the entire connection (for delete / updating / insert queries), False if you just want the cursor (select queries) 
def newDatabaseConnection(b=False):
    dsn_tns = cx_Oracle.makedsn(os.getenv('DB_HOST_IP'),os.getenv('DB_PORT'),service_name=os.getenv('DB_SERVICE_NAME'))
    connection = cx_Oracle.connect(os.getenv('DB_USER'),os.getenv('DB_PASSWD'),dsn_tns)
    return connection if b else connection.cursor()

def customerOrBarExists(c, usertype, username):
    query = "SELECT * FROM {} WHERE username = :username".format(usertype)
    c.execute(query, {'username': username})
    result = c.fetchone()
    print("about to return: {}".format(result))
    return result
    
def validateCustomerOrBar(userrow, password):
    pass_hash = userrow[2]
    return sha256_crypt.verify(password, pass_hash)

def buildSessionDict(result):
    info = result['info']
    return_dict = {'ID': info[0], 'USERNAME': info[1], 'TYPE': result['type']}
    if result['type'] == 'bar':
        return_dict['NAME'] = info[3]
        return_dict['STREET_ADDRESS'] = info[4]
        return_dict['CITY'] = info[5]
        return_dict['STATE'] = info[6]
        return_dict['ZIP'] = info[7]
        return_dict['PHONE'] = info[8]
        return_dict['EMAIL'] = info[9]
        return_dict['WEBSITE_URL'] = info[10]
        print(return_dict['WEBSITE_URL'])
    else: # customer
        return_dict['NAME'] = info[3] + " " + info[4]
        return_dict['HAS_PROFILE_PICTURE'] = bool(info[5])
    return return_dict


def buildBarVariable(session):
    bar_variables = ['ID', 'NAME', 'USERNAME', 'STREET_ADDRESS', 'CITY', 'STATE', 'ZIP', 'PHONE', 'EMAIL', 'WEBSITE_URL']
    bar = {b: (session[b] if b in session and session[b] != None else "") for b in bar_variables}
    print("sending the template bar: {}".format(bar))
    profile_all_filled_out = functools.reduce(lambda x, y: x and bar[y] != "", bar_variables)
    bar["COMPLETE_PROFILE"] = profile_all_filled_out
    return bar

def delete_previous_day_purchases():
    conn = newDatabaseConnection(True)
    c = conn.cursor()
    c.execute('delete from purchases where purchase_id in (select purchase_id from purchases where drink_picked_up = 0)')
    c.execute('delete from purchase_items where purchase_id in (select purchase_id from purchases where drink_picked_up = 0)')
    conn.commit() # necessary for deletes 
    print('executed daily queries!')

def getCustomer(session):
    c = newDatabaseConnection(False)
    query_string = "select c.first_name, c.last_name, c.username, c.cust_id, c.profile_picture from customers c where c.username = :username"
    c.execute(query_string, [session['USERNAME']])
    customer = {}
    cust = c.fetchone()
    customer["firstname"] = cust[0]
    customer["lastname"] = cust[1]
    customer["fullname"] = cust[0] + ' ' + cust[1]
    customer["username"] = cust[2]
    customer["cust_id"] = cust[3]
    customer["profile_picture"] = cust[4]
    return customer

def getIngredients(item_id):
    c = newDatabaseConnection(False)
    query_string = "select i.name, i.ingredient_id from item_ingredients ii, ingredients i where ii.ingredient_id = i.ingredient_id and ii.item_id = :id"    
    c.execute(query_string, [item_id])
    ingreds = []
    for row in c:
        i = {}
        i["name"] = row[0]
        i["id"] = row[1]
        ingreds.append(i)
    return ingreds


def getMenu(session, checkins):
    menus = []
    for checkin in checkins:
        bid = checkin["bar_id"]
        bn = checkin["bar_name"]
        c = newDatabaseConnection(False)
        query_string = "select d.price, d.note, d.size_ounces, d.container, i.name, i.brand, i.drink_type, i.item_id, d.drink_id from items i, drinks d where d.item_id = i.item_id and d.bar_id = :bid"
        c.execute(query_string, [bid])
        menu = {}
        menu['bar_id'] = bid
        menu['bar_name'] = bn
        menu['drinks'] = {}
        for row in c:
            item_id = row[7]
            if item_id in menu['drinks'].keys():
                style = {}
                style["price"] = row[0]
                style["pricestr"] = '{:.2f}'.format(row[0])
                style["note"] = row[1]
                if row[2] != 0:
                    style["size"] = row[2]
                if row[3] != 'N/A':
                    style["container"] = row[3]
                style["drink_id"] = row[8]
                menu['drinks'][item_id]["style"].append(style)
                continue
            item = {}
            item["style"] = []
            style = {}
            style["price"] = row[0]
            style["pricestr"] = '{:.2f}'.format(row[0])
            style["note"] = row[1] or ""
            if row[2] != 0:
                style["size"] = row[2]
            if row[3] != 'N/A':
                style["container"] = row[3]
            style["drink_id"] = row[8]
            item["style"].append(style)
            item["currency"] = '$'
            item["name"] = row[4]
            item["brand"] = row[5]
            item["type"] = row[6]
            item["item_id"] = row[7]
            item["ingredients"] = getIngredients(item["item_id"])
            menu['drinks'][row[7]] = item
        menus.append(menu)
    return menus

def getCheckIns(session, cid):
    c = newDatabaseConnection(False)
    now = datetime.now()
    query_string = "select c.bar_id, c.start_date, c.end_date, c.price, c.duration, c.duration_unit, b.name from checkins c, bars b where c.cust_id = :cid and c.start_date < :now and c.end_date > :now and b.bar_id = c.bar_id"
    c.execute(query_string, [cid, now])
    checkins = []
    for row in c:
        checkin = {}
        checkin["bar_id"] = row[0]
        checkin["start_date"] = row[1]
        checkin["end_date"] = row[2]
        checkin["price"] = row[3]
        checkin["duration"] = row[4]
        checkin["duration_unit"] = row[5]
        checkin["bar_name"] = row[6]
        checkins.append(checkin)
    return checkins

def getBars():
    c = newDatabaseConnection(False)
    query_string = "select b.bar_id, b.street_address, b.name, b.username, b.zip, b.phone_number, b.email, b.city, b.state, b.website_url, b.profile_picture from bars b"
    c.execute(query_string)
    bars = []
    for row in c:
        #print('row:', row)
        bar = {}
        bar["bar_id"] = row[0]
        bar["street_address"] = row[1]
        bar["name"] = row[2]
        bar["username"] = row[3]
        bar["zip"] = row[4]
        bar["phone_number"] = row[5]
        bar["email"] = row[6]
        bar["city"] = row[7]
        bar["state"] = row[8]
        bar["website_url"] = row[9] or ""
        bar["profile_picture"] = row[10]
        r = json.dumps(bar)
        bar['string'] = r
        bars.append(bar)
    return bars

def redirectLogin(session):
    print('directing login...')
    customer = getCustomer(session)
    checkins = getCheckIns(session, customer["cust_id"])
    if len(checkins) > 0:
        session["CHECKED_IN"] = True
        #menus = getMenu(session, checkins)
        return "customer" 
    else:
        session["CHECKED_IN"] = False
        return "checkin" 

def getPlans():
    plans = []
    types = ["1", "2", "3"]
    prices = [7, 13, 16]
    names = ["One Night", "One Night @ All Bars", "One Week"]
    dns = [1, 1, 1]
    dnus = ["day", "day", "week"]
    alls = [False, True, False]
    deltas = [1, 1, 7] 
    for t,p,n,d,u,a,x in zip(types, prices, names, dns, dnus, alls, deltas):
        plan = {}
        plan["type"] = t
        plan["price"] = p
        plan["name"] = n
        plan["duration"] = d
        plan["duration_unit"] = u
        plan["allbars"] = a
        plan["delta"] = x
        z = json.dumps(plan)
        plan["string"] = z
        plans.append(plan)
    return plans

def getCarts(session, cust_id):
    c = newDatabaseConnection(False)
    query_string = "select c.bar_id, c.cust_id, c.cart_id, c.cart_date, c.cost, b.name from carts c, bars b where c.cust_id = :cid and c.bar_id = b.bar_id"
    c.execute(query_string, [cust_id])
    carts = []
    for ct in c:
        cart = {}
        cart['bar_id'] = ct[0]
        cart['cust_id'] = ct[1]
        cart['cart_id'] = ct[2]
        cart['cart_date'] = ct[3]
        cart['cost'] = ct[4]
        cart['bar_name'] = ct[5]
        drinks = []
        q = newDatabaseConnection(False)
        query_string = "select c.drink_id, c.quantity, d.price, d.container, d.size_ounces, d.note, i.name, c.note from cart_drinks c, drinks d, items i where c.cart_id = :cid and c.drink_id = d.drink_id and i.item_id = d.item_id"
        q.execute(query_string, [cart['cart_id']])
        for d in q:
            drink = {}
            drink['drink_id'] = d[0]
            drink['quantity'] = d[1]
            drink['price'] = d[2]
            drink['container'] = d[3]
            drink['size_ounces'] = d[4]
            drink['bar_note'] = d[5]
            drink['name'] = d[6]
            drink['cust_note'] = d[7]
            drinks.append(drink)
        cart['drinks'] = drinks
        carts.append(cart)
    return carts

def getOpenPurchases(customer):
    d2 = datetime.now()
    d1 = formatDate(d2 - timedelta(days=1))
    d2 = formatDate(d2)
    c = newDatabaseConnection(False)
    query_string = "select p.bar_id, p.cust_id, p.purchase_id, p.drink_prepared, p.drink_picked_up, p.order_time, p.final_cost, p.final_tip, b.name from purchases p, bars b where p.cust_id = :cid and b.bar_id = p.bar_id and p.order_time > TO_TIMESTAMP(:d1, 'YYYY-MM-DD-HH24-MI-SS') and p.order_time < TO_TIMESTAMP(:d2, 'YYYY-MM-DD-HH24-MI-SS')"
    c.execute(query_string, [customer['cust_id'], d1, d2])
    purchases = []
    for p in c:
        purchase = {}
        purchase['bar_id'] = p[0]
        purchase['bar_name'] = p[8]
        purchase['cust_id'] = p[1]
        purchase['purchase_id'] = p[2]
        purchase['drink_prepared'] = p[3]
        purchase['picked_up'] = p[4]
        purchase['order_time'] = p[5]
        #d = datetime.strptime(, '%Y-%m-%d %H:%M:%S')
        purchase['order_time_str'] = datetime.strftime(p[5] - timedelta(hours=4), '%A, %B %d @ %I:%M%p')
        purchase['cost'] = p[6]
        purchase['tip'] = p[7]
        purchases.append(purchase)
    return purchases

def ordersData(session):
    customer = getCustomer(session)
    purchases = getOpenPurchases(customer)
    return render_template('orders.html', customer=customer, purchases=purchases)

def checkoutData(session):
    customer = getCustomer(session)
    checkins = getCheckIns(session, customer["cust_id"])
    carts = getCarts(session, customer['cust_id'])
    if len(carts) < 1:
        createCart(session, {"bar_id": 1}, customer, formatDate(datetime.now()))
        carts = getCarts(session, customer['cust_id'])
    purchases = getOpenPurchases(customer)
    return render_template('checkout.html', customer=customer, carts=carts, checkins=checkins, purchases=purchases)

def checkinData(session):
    customer = getCustomer(session)
    checkins = getCheckIns(session, customer["cust_id"])
    bars = getBars()
    plans= getPlans()
    return render_template('checkin.html', customer=customer, checkins=checkins, bars=bars, plans=plans)

def custDashboardData(session):
    customer = getCustomer(session)
    checkins = []
    checks = 0
    while len(checkins) < 1 and checks < 15:
        checkins = getCheckIns(session, customer["cust_id"])
        print('checkins:', checkins)
        time.sleep(.1)
        checks += 1
    menus = getMenu(session, checkins)
    return render_template('customer.html', customer=customer, menus=menus, checkins=checkins)

def deleteDrink(update):
    con = newDatabaseConnection(True)
    delete_string = "DELETE from CART_DRINKS where cart_id = :cid and drink_id = :did"
    cur = con.cursor()
    cur.execute(delete_string, [update['cart_id'], update['drink_id']])
    con.commit()
    recountCarts(session, update['cart_id'])

def updateDrink(update):
    con = newDatabaseConnection(True)
    update_string = "UPDATE CART_DRINKS set quantity = :qy where cart_id = :cid and drink_id = :did"
    cur = con.cursor()
    cur.execute(update_string, [update['quantity'], update['cart_id'], update['drink_id']])
    con.commit()
    recountCarts(session, update['cart_id'])

def addToCart(session, order):
    customer = getCustomer(session)
    cart = getCart(customer, order)
    session['BAR_ID'] = cart['bar_id'] 

    drinkInCart = False
    for d in cart['drinks']:
        if order['drink'] == d['drink_id']:
            drinkInCart = True

    if drinkInCart:
        updateCartCount(cart['cart_id'], order['drink'], order['quantity'], order['note'])
    else:
        con = newDatabaseConnection(True)
        addto_cart = "INSERT into CART_DRINKS (drink_id, cart_id, quantity, note) VALUES (:did, :cid, :qy, :note)"
        cur = con.cursor()
        # create row in cart_drinks table
        print(order['drink'], cart['cart_id'], order['quantity'], order['note'])
        cur.execute(addto_cart, [order['drink'], cart['cart_id'], order['quantity'], order['note']])
        con.commit()
    updateCart(cart['cart_id'], order['drink'], order['quantity'])
    return redirect(url_for('customer'))

def cartToPurchase(cart_id, tip):
    recountCarts(session, cart_id)
    createPurchase(cart_id, tip)
    clearCart(cart_id)

def createPurchase(cart_id, tip):
    cart = getCartByID(cart_id)
    print('initial cart before purchase', cart)
    con = newDatabaseConnection(True)
    add_purchase = "INSERT into PURCHASES (cust_id, bar_id, final_tip, final_cost, drink_picked_up, drink_prepared, order_time, purchase_method, customer_ordered) VALUES (:csid, :bid, :tip, :cost, :dpu, :dp, TO_TIMESTAMP(:otime, 'YYYY-MM-DD-HH24-MI-SS'), :pm, :co)"
    cur = con.cursor()
    otime = formatDate(datetime.now())
    # create row in carts table
    cur.execute(add_purchase, [cart['cust_id'], cart['bar_id'], float(tip), cart['cost'], 0, 0, otime, 'Card', 1])
    con.commit()
    createPurchaseDrinks(cart, otime)

def getPurchaseByTime(otime, cart):
    p = None
    while p is None:
        c = newDatabaseConnection(False)
        query_string = "select p.bar_id, p.cust_id, p.purchase_id, p.final_tip, p.final_cost from purchases p where p.order_time = TO_TIMESTAMP(:otime, 'YYYY-MM-DD-HH24-MI-SS') and p.cust_id = :cid"
        c.execute(query_string, [otime, cart['cust_id']])
        p = c.fetchone()
        time.sleep(.1)
    return p

def createPurchaseDrinks(cart, otime):
    p = getPurchaseByTime(otime, cart)
    purchase = {}
    purchase['bar_id'] = p[0]
    purchase['cust_id'] = p[1]
    purchase['purchase_id'] = p[2]
    purchase['tip'] = p[3]
    purchase['cost'] = p[4]
    for drink in cart['drinks']:
        con = newDatabaseConnection(True)
        add_purchasedrink = "INSERT into PURCHASE_DRINKS (purchase_id, drink_id, quantity, note) VALUES (:pid, :did, :qy, :note)"
        cur = con.cursor()
        print('here')
        print(purchase['purchase_id'])
        print(drink)
        print('there')
        cur.execute(add_purchasedrink, [purchase['purchase_id'], drink['drink_id'], drink['quantity'], drink['cust_note']])
        con.commit()

def clearCart(cart_id):
    con = newDatabaseConnection(True)
    delete_string = "DELETE from CART_DRINKS where cart_id = :cid"
    cur = con.cursor()
    cur.execute(delete_string, [cart_id])
    con.commit()
    recountCarts(session, cart_id)

def getCartByID(cart_id):
    c = newDatabaseConnection(False)
    query_string = "select c.bar_id, c.cust_id, c.cart_id, c.cart_date, c.cost from carts c where c.cart_id= :cid"
    c.execute(query_string, [cart_id])
    cart = {}
    ct = c.fetchone()
    cart['bar_id'] = ct[0]
    cart['cust_id'] = ct[1]
    cart['cart_id'] = ct[2]
    cart['cart_date'] = ct[3]
    cart['cost'] = ct[4]
    drinks = []
    z = newDatabaseConnection(False)
    query_string = "select c.drink_id, c.quantity, d.price, d.container, d.size_ounces, d.note, i.name, c.note from cart_drinks c, drinks d, items i where c.cart_id = :caid and d.drink_id = c.drink_id and d.item_id = i.item_id"
    z.execute(query_string, [cart['cart_id']])
    for d in z:
        drink = {}
        drink['drink_id'] = d[0]
        drink['quantity'] = d[1]
        drink['price'] = d[2]
        drink['container'] = d[3]
        drink['size_ounces'] = d[4]
        drink['bar_note'] = d[5]
        drink['name'] = d[6]
        drink['cust_note'] = d[7]
        drinks.append(drink)
    cart['drinks'] = drinks
    return cart

def recountCarts(session, cart_id):
    customer = getCustomer(session)
    carts = getCarts(session, customer['cust_id'])
    cart_id = int(cart_id)
    for cart in carts:
        if cart['cart_id'] != cart_id:
            continue
        cost = 0
        for drink in cart['drinks']:
            cost += drink['price'] * drink['quantity']
        con = newDatabaseConnection(True)
        update_string = "UPDATE CARTS set cost = :cost where cart_id = :cid"
        cur = con.cursor()
        cur.execute(update_string, [cost, cart_id])
        con.commit()

def updateCartCount(cart_id, drink_id, quantity, note):
    con = newDatabaseConnection(True)
    update_string = "UPDATE CART_DRINKS set quantiy = quantity + :qy, set note = note + :note where cart_id = :cid and drink_id = :did"
    cur = con.cursor()
    cur.execute(update_string, [quantity, note, cart_id, drink_id])
    con.commit()

def updateCart(cart_id, drink_id, quantity):
    drink = getDrink(drink_id)
    con = newDatabaseConnection(True)
    update_string = "UPDATE CARTS set cost = cost + :price where cart_id = :cid"
    cur = con.cursor()
    cur.execute(update_string, [drink['price']*float(quantity), cart_id])
    con.commit()

def getDrink(drink_id):
    c = newDatabaseConnection(False)
    query_string = "select d.drink_id, d.bar_id, d.container, d.note, d.price, d.size_ounces, d.item_id from drinks d where d.drink_id = :did"
    c.execute(query_string, [drink_id])
    drink = {}
    d = c.fetchone()
    drink['drink_id'] = d[0]
    drink['bar_id'] = d[1]
    drink['container'] = d[2]
    drink['note'] = d[3]
    drink['price'] = d[4]
    drink['size_ounces'] = d[5]
    drink['item_id'] = d[6]
    return drink
     

def getCart(customer, order):
    c = newDatabaseConnection(False)
    query_string = "select c.bar_id, c.cust_id, c.cart_id, c.cart_date, c.cost from carts c where c.bar_id = :bid and c.cust_id = :cid"
    c.execute(query_string, [order['bar'], customer['cust_id']])
    cart = {}
    ct = c.fetchone()
    cart['bar_id'] = ct[0]
    cart['cust_id'] = ct[1]
    cart['cart_id'] = ct[2]
    cart['cart_date'] = ct[3]
    cart['cost'] = ct[4]
    drinks = []
    c = newDatabaseConnection(False)
    query_string = "select c.drink_id from cart_drinks c where c.cart_id = :caid"
    c.execute(query_string, [cart['cart_id']])
    for d in c:
        drink = {}
        drink['drink_id'] = d[0]
        drinks.append(drink)    
    cart['drinks'] = drinks
    return cart
    

def createCart(session, bar, cust, date):
    con = newDatabaseConnection(True)
    add_cart = "INSERT into CARTS (cust_id, bar_id, cart_date, cost) VALUES (:cid, :bid, TO_DATE(:cdate, 'YYYY-MM-DD-HH24-MI-SS'), :cost)"
    cur = con.cursor()
    # create row in carts table
    cur.execute(add_cart, [cust['cust_id'], bar['bar_id'], date, 0])
    con.commit()

def createCheckin(checkins, session):
    customer = getCustomer(session)
    bars = []
    if checkins["allbars"] == True:
        bars = getBars()
    else:
        bars.append({"bar_id": checkins["bar_id"]})
    for b in bars:
        bid = b["bar_id"]
        createCart(session, b, customer, checkins['start_date'])
        con = newDatabaseConnection(True)
        add_checkin = "INSERT into CHECKINS (bar_id, cust_id, start_date, end_date, price, duration, duration_unit) VALUES (:bid, :cid, TO_DATE(:sd, 'YYYY-MM-DD-HH24-MI-SS'), TO_DATE(:ed, 'YYYY-MM-DD-HH24-MI-SS'), :p, :dn, :dnu)"
        cur = con.cursor()
        # create row in checkins table
        cur.execute(add_checkin, [bid, customer['cust_id'], checkins['start_date'], checkins['end_date'], checkins['price'], checkins['duration'], checkins['duration_unit']])
        con.commit()
    return redirect(url_for('customer'))

def formatDate(z):
    d = datetime.strftime(z, '%Y-%m-%d-%H-%M-%S')
    return d

def barDashboardData(htmlfile, session):
    c = newDatabaseConnection(False)
    earliest_drinks_date = getEarliestDrinksDate()
    
    query_string = "select p.*, c.username, c.first_name, c.last_name, c.profile_picture, LISTAGG(TO_CHAR(pd.drink_id) || ',' || TO_CHAR(pd.quantity) || ',' || TO_CHAR(pd.note) || ',' || TO_CHAR(d.size_ounces) || ',' || d.container || ',' || TO_CHAR(d.item_id) || ',' || i.name || ',' || TO_CHAR(i.profile_picture), '|') WITHIN GROUP (ORDER BY pd.drink_id) drink_info FROM purchases p inner join purchase_drinks pd on p.purchase_id = pd.purchase_id inner join drinks d on pd.drink_id = d.drink_id inner join items i on d.item_id = i.item_id inner join customers c on c.cust_id = p.cust_id where p.bar_id = :bid and p.order_time > (select TO_TIMESTAMP(:ts, 'YYYY-MM-DD HH24:MI:SS') FROM dual) GROUP BY p.purchase_id, p.cust_id, p.bar_id, p.order_time, p.final_cost, p.final_tip, p.purchase_method, p.customer_ordered, p.drink_prepared, p.drink_picked_up, c.username, c.first_name, c.last_name, c.profile_picture ORDER BY p.order_time ASC"
    c.execute(query_string, [session['ID'], earliest_drinks_date])
    c.rowfactory = makeDictFactory(c)
    purchases = c.fetchall()
    readable_purchases = list(map(clarifyPurchaseDict, purchases))
    drinks_ordered = list(filter(lambda d: d["CUSTOMER_ORDERED"] and not d["DRINK_PREPARED"] and not d["DRINK_PICKED_UP"],readable_purchases))
    drinks_prepared = list(filter(lambda d: d["CUSTOMER_ORDERED"] and d["DRINK_PREPARED"] and not d["DRINK_PICKED_UP"],readable_purchases))

    print("drinks_ordered: {}\n\ndrinks_prepared: {}".format(drinks_ordered, drinks_prepared))  
    return render_template(htmlfile, drinks_ordered=drinks_ordered, drinks_prepared=drinks_prepared)

def getEarliestDrinksDate():
    now = datetime.now() - timedelta(hours=4) # necessary to adjust from UTC time
    print(now.hour)
    if now.hour < 12:
        d = datetime(now.year,now.month, now.day - 1, 12)
    else:
        d = datetime(now.year,now.month, now.day, 12)
    return d.strftime("%Y-%m-%d %H:%M:%S")

def datetimeToTimeString(dt_object):
    if dt_object is None:
        return '#'
    return dt_object.strftime('%-I:%M %p')

def datetimeToDatetimeString(dt_object):
    if dt_object is None:
        return '#' 
    return dt_object.strftime('%m-%d-%Y, %-I:%M %p')

def datetimeStringToDatetime(string): 
    return datetime.strptime(string, '%m/%d/%Y %I:%M %p')

def timeStringToDatetime(string):
    return datetime.strptime(string, '%I:%M %p') 

def clarifyPurchaseDict(d):
    d['ORDER_TIME'] = datetimeToTimeString(d['ORDER_TIME'])
    d['TOTAL'] = dollarStringFromFloat(d['FINAL_COST'] + d['FINAL_TIP'])
    d['FINAL_COST'] = dollarStringFromFloat(d['FINAL_COST'])
    d['FINAL_TIP'] = dollarStringFromFloat(d['FINAL_TIP']) 
    return d

def dollarStringFromFloat(f):
    return '{:,.2f}'.format(f)

## TODO / if we have time. Below = not correct but a start...:::
def validateImageFile(bar_pictures, bid):
    pass
    #image_metadata = subprocess.check_output("file {}".format(os.path.join(bar_pictures, str(bid) + ".jpg")))
    #print("image_metadata:{}".format(image_metadata)) 

def updateMenuData(filename, order_by, session):
    c = newDatabaseConnection(False)
    c.execute('select d.drink_id, i.item_id, i.name, i.brand, i.profile_picture, d.size_ounces, d.container, tmp2.n_specials, d.price from drinks d inner join items i on d.item_id = i.item_id inner join (select d.drink_id, NVL(tmp.ct, 0) n_specials from drinks d left outer join (select drink_id, count(*) ct from specials group by drink_id) tmp on d.drink_id = tmp.drink_id order by drink_id) tmp2 on d.drink_id = tmp2.drink_id where d.bar_id = :bid order by {}'.format(order_by), [session['ID']])
    menu_items = list(map(lambda x: x[0:8] + (dollarStringFromFloat(x[8]),) , [row for row in c]))
    return render_template(filename, menu_items=menu_items)

def findMenuOrder(sorting_method):
    if sorting_method == "item-name-a-z": 
        sql_string = "name ASC" 
    elif sorting_method == "item-name-z-a": 
        sql_string = "name DESC" 
    elif sorting_method == "cheapest-items-first": 
        sql_string = "price ASC" 
    elif sorting_method == "expensive-items-first": 
        sql_string = "price DESC" 
    elif sorting_method == "specials-first": 
        sql_string = "n_specials DESC" 
    elif sorting_method == "specials-last": 
        sql_string = "n_specials ASC" 
    else: 
        print("we're being hacked...") 
        sql_string = "name ASC"
    return sql_string  

def makeDictFactory(cursor):
    columnNames = [d[0] for d in cursor.description]
    def createRow(*args):
        return dict(zip(columnNames, args))
    return createRow

def getChartRange(c):
    if c == "last_week":
        return "order_time > CURRENT_TIMESTAMP - 7 AND"
    elif c == "last_month":
        return "order_time > CURRENT_TIMESTAMP - 30 AND"
    else:
        return ""

def someLettersAndNumbers(text):
    return any(char.isdigit() for char in text) and any(char.isalpha() for char in text)
